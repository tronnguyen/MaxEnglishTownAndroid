package cnpm.khoaluan.maxsenglishtown.global

import android.app.Activity
import android.content.Context
import android.util.Log
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import org.xmlpull.v1.XmlPullParserFactory
import java.util.*
import java.io.*




/**
 * Created by Nguyen Van Tron on 3/29/2018.
 */
class Global{
    companion object {
        private var instance : Global? = null
        var data : Dictionary<String,String> = Hashtable()
        fun Instance() : Global{
            if(instance == null)
                instance = Global()
            return instance as Global
        }
        fun ParseXMLToHashTable(id : String, mActivity : Activity) : Dictionary<String, String>? {
            val xmlPullParserFactory: XmlPullParserFactory
            try {
                xmlPullParserFactory = XmlPullParserFactory.newInstance()
                val xmlPullParser = xmlPullParserFactory.newPullParser()
                val `is` = mActivity.assets.open(id)
                xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
                xmlPullParser.setInput(`is`, null)
                return ProcessParsingToHashtable(xmlPullParser)
            } catch (e: XmlPullParserException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            return null
        }
        //Parse XML
        fun ParseXML(id : String, mActivity : Activity, flag : Boolean) : ArrayList<String>? {
            val xmlPullParserFactory: XmlPullParserFactory
            try {
                xmlPullParserFactory = XmlPullParserFactory.newInstance()
                val xmlPullParser = xmlPullParserFactory.newPullParser()
                val `is` = mActivity.assets.open(id)
                xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
                xmlPullParser.setInput(`is`, null)
                if(flag)
                    return ProcessParsing(xmlPullParser)
                else
                    return GetXXX(xmlPullParser)
            } catch (e: XmlPullParserException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            return null
        }
        @Throws(XmlPullParserException::class, IOException::class)
        private fun ProcessParsing(xmlPullParser: XmlPullParser) : ArrayList<String>{
            var list : ArrayList<String> = ArrayList()
            var eventType = xmlPullParser.eventType
            while (eventType != XmlPullParser.END_DOCUMENT) {
                var eltName: String?
                when (eventType) {
                    XmlPullParser.START_TAG -> {
                        eltName = xmlPullParser.name
                        if (eltName!!.endsWith("name")) {
                            val value = xmlPullParser.nextText()
                            list.add(value)
                        }
                    }
                }
                eventType = xmlPullParser.next()
            }
            list.sort()
            return list
        }

        @Throws(XmlPullParserException::class, IOException::class)
        private fun GetXXX(xmlPullParser: XmlPullParser) : ArrayList<String>{
            var list : ArrayList<String> = ArrayList()
            var eventType = xmlPullParser.eventType
            while (eventType != XmlPullParser.END_DOCUMENT) {
                var eltName: String?
                when (eventType) {
                    XmlPullParser.START_TAG -> {
                        eltName = xmlPullParser.name
                        if (eltName!!.endsWith("ImageTarget")) {
                            val value = xmlPullParser.getAttributeValue(0)
                            list.add(value)
                        }
                    }
                }
                eventType = xmlPullParser.next()
            }
            list.sort()
            return list
        }
        //
        @Throws(XmlPullParserException::class, IOException::class)
        private fun ProcessParsingToHashtable(xmlPullParser: XmlPullParser) : Dictionary<String,String> {
            var list : Dictionary<String,String> = Hashtable()
            var flag = false
            var eventType = xmlPullParser.eventType
            while (eventType != XmlPullParser.END_DOCUMENT) {
                var eltName: String?
                when (eventType) {
                    XmlPullParser.START_TAG -> {
                        eltName = xmlPullParser.name
                        if (eltName!!.endsWith("Word")) {
                               flag = true

                        }
                    }
                }
                eventType = xmlPullParser.next()
                if(flag){
                    when (eventType) {
                        XmlPullParser.START_TAG -> {
                            flag = false
                            val tag = xmlPullParser.name
                            var value = xmlPullParser.nextText()
                            list.put(tag, value)
                        }
                    }

                }
            }
            return list
        }
        fun saveData(context: Context, mJsonResponse: String) : String? {
            val `is` = context.assets.open(Constants.DATA_FILE_NAME)
            val size = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            var json = String(buffer)
            var myjsonstring = json
            if(mJsonResponse != ""){
                myjsonstring = mJsonResponse
            }
            try {
                val file = FileWriter(context.filesDir.path + "/" + Constants.DATA_FILE_NAME)
                file.write(myjsonstring)
                file.flush()
                file.close()
            } catch (e: IOException) {
                Log.e("TAG", "Error in Writing: " + e.localizedMessage)
            }
            return  myjsonstring
        }

        fun getData(context: Context): String? {
            try {
                val f = File(context.filesDir.path + "/" + Constants.DATA_FILE_NAME)

                //check whether file exists
                if(!f.exists()){
                    return saveData(context,"")
                }
                val `is` = FileInputStream(f)
                val size = `is`.available()
                if(size <=0){
                    `is`.close()
                    return saveData(context,"")
                }
                val buffer = ByteArray(size)
                `is`.read(buffer)
                `is`.close()
                return String(buffer)
            } catch (e: IOException) {
                Log.e("TAG", "Error in Reading: " + e.localizedMessage)
                return null
            }

        }
        fun saveDataRewards(context: Context, mJsonResponse: String) : String? {
            val `is` = context.assets.open(Constants.DATA_REWARDS)
            val size = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            var json = String(buffer)
            var myjsonstring = json
            if(mJsonResponse != ""){
                myjsonstring = mJsonResponse
            }
            try {
                val file = FileWriter(context.filesDir.path + "/" + Constants.DATA_REWARDS)
                file.write(myjsonstring)
                file.flush()
                file.close()
            } catch (e: IOException) {
                Log.e("TAG", "Error in Writing: " + e.localizedMessage)
            }
            return  myjsonstring
        }

        fun getDataRewards(context: Context): String? {
            try {
                val f = File(context.filesDir.path + "/" + Constants.DATA_REWARDS)

                //check whether file exists
                if(!f.exists()){
                    return saveDataRewards(context,"")
                }
                val `is` = FileInputStream(f)
                val size = `is`.available()
                if(size <=0){
                    `is`.close()
                    return saveDataRewards(context,"")
                }
                val buffer = ByteArray(size)
                `is`.read(buffer)
                `is`.close()
                return String(buffer)
            } catch (e: IOException) {
                Log.e("TAG", "Error in Reading: " + e.localizedMessage)
                return null
            }

        }
        fun saveDataCafe(context: Context, mJsonResponse: String) : String? {
            val `is` = context.assets.open(Constants.DATA_CAFE)
            val size = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            var json = String(buffer)
            var myjsonstring = json
            if(mJsonResponse != ""){
                myjsonstring = mJsonResponse
            }
            try {
                val file = FileWriter(context.filesDir.path + "/" + Constants.DATA_CAFE)
                file.write(myjsonstring)
                file.flush()
                file.close()
            } catch (e: IOException) {
                Log.e("TAG", "Error in Writing: " + e.localizedMessage)
            }
            return  myjsonstring
        }
        fun getDataCafe(context: Context): String? {
            try {
                val f = File(context.filesDir.path + "/" + Constants.DATA_CAFE)

                //check whether file exists
                if(!f.exists()){
                    return saveDataCafe(context,"")
                }
                val `is` = FileInputStream(f)
                val size = `is`.available()
                if(size <=0){
                    `is`.close()
                    return saveDataCafe(context,"")
                }
                val buffer = ByteArray(size)
                `is`.read(buffer)
                `is`.close()
                return String(buffer)
            } catch (e: IOException) {
                Log.e("TAG", "Error in Reading: " + e.localizedMessage)
                return null
            }

        }
    }

}