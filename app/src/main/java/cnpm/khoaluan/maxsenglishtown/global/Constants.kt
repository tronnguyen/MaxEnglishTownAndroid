package cnpm.khoaluan.maxsenglishtown.global

import android.app.Activity
import android.content.Context
import com.facebook.rebound.SpringSystem
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by Nguyen Van Tron on 4/3/2018.
 */
class Constants {
    companion object {
        var rewards =
                booleanArrayOf(false,false,false,false,
                        false,false,false,false,
                        false,false,false,false)
        var packAlphabet : ArrayList<String>? = null
        var packIDAnimal : ArrayList<String>? = null
        var jsonArrayData : JSONArray? = null
        const val PATH_IMAGES = "images/"
        const val PATH_IMAGES_BUTTON = "images/buttons/"
        const val PATH_IMAGES_OTHERS = "images/others/"
        const val TUTORIAL_SCREEN_ID = 100
        const val WELCOME_SCREEN_ID = 101
        const val BOARD_SCREEN_ID = 102
        const val SCHOOL_SCREEN_ID = 103
        const val CARD_SCREEN_ID = 104
        const val LESSON_SCREEN_ID = 105
        const val PRACTICE_SCREEN_ID = 106
        const val GUESS_THE_WORD_SCREEN_ID = 107
        const val MEMORY_MATCH_SCREEN_ID = 108
        const val MAP_SCREEN_ID = 109
        const val LIBRARY_SCREEN_ID = 110
        const val CAFE_SCREEN_ID = 111
        const val PARK_SCREEN_ID = 112
        const val LOADING_SCREEN_ID =  113
        var SpringSystemManager = SpringSystem.create()
        var SCREEN_ID = 113
        var CARD :  String = "alphabet_a"
        var DATA_FILE_NAME : String = "learn_alphabet.json"
        var DATA_REWARDS : String = "rewards.json"
        var DATA_CAFE : String = "cafe_mode.json"
        var PACK_ID : String = "data_alphabet.xml"
        val RES_LOCKEDCARD = "lockedcardv3"
        val RES_FONTOFCARD = "frontofcard"
        val PACK_FILE : String = "data_pack.xml"
        val PATH_IMAGES_ALPHABET = "alphabetImages/"
        val PATH_IMAGES_ANIMALS = "animalImages/"
        var BEFORE_CARD = "frontofcard"
        var NOT_MATCH = false
        var IS_DONE = 0
        var TEXT_DONE = ""
        //DataSaved
        var jsonObjectData : JSONObject? = null
        var jsonObjectRewards : JSONObject? = null
        var jsonArrayRewards : JSONArray? = null
       fun loadData(activity : Activity){
           PACK_ID = "data_animals.xml"
           packIDAnimal = Global.ParseXML(PACK_ID, activity,false)
           PACK_ID = "data_alphabet.xml"
           packAlphabet = Global.ParseXML(PACK_ID, activity,false)

       }
        fun saveDataRewards(index: Int, context: Context) {
            jsonObjectRewards!!.getJSONArray("rewards").getJSONObject(index).remove("trophy" + (index + 1).toString())
            jsonObjectRewards!!
                    .getJSONArray("rewards")
                    .getJSONObject(index)
                    .put("trophy" + (index + 1).toString(), true)
            var str = jsonObjectRewards.toString()
            Global.saveDataRewards(context, str)
        }
        fun getValue(id : Int, name : String) : Boolean{
            return jsonArrayRewards!!.getJSONObject(id).getBoolean(name)
        }
    }
}