package cnpm.khoaluan.maxsenglishtown.model

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import cnpm.khoaluan.maxsenglishtown.R

/**
 * Created by Nguyen Van Tron on 6/11/2018.
 */

class RewardDialog : Dialog, View.OnClickListener{
    private var type : Boolean = false
    private var number  :Int = 0
    private var activity : Activity? = null
    private var titleBadges : IntArray = intArrayOf(R.string.tite_badge1,
            R.string.tite_badge2,
            R.string.tite_badge3,
            R.string.tite_badge4,
            R.string.tite_badge5,
            R.string.tite_badge6,
            R.string.tite_badge7,
            R.string.tite_badge8,
            R.string.tite_badge9,
            R.string.tite_badge10,
            R.string.tite_badge11,
            R.string.tite_badge12
            )
    private var badges : IntArray = intArrayOf(R.string.badge1,
            R.string.badge2,
            R.string.badge3,
            R.string.badge4,
            R.string.badge5,
            R.string.badge6,
            R.string.badge7,
            R.string.badge8,
            R.string.badge9,
            R.string.badge10,
            R.string.badge11,
            R.string.badge12
    )
    constructor(activity: Activity, type : Boolean, number : Int) : super(activity) {
        this.activity = activity
        this.type = type
        this.number = number
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.reward_dialog)
        var btnGreat : Button = findViewById(R.id.btn_great)
        var tvCongrats : TextView = findViewById(R.id.tv_congrats)
        var tvTitle : TextView = findViewById(R.id.tv_title)
        var tvDescription : TextView = findViewById(R.id.tv_description)
        var imgTrophy : ImageView = findViewById(R.id.iv_reward)
        if(type){
            btnGreat.text = "Great!"
            tvCongrats.text = activity!!.getString(R.string.congrats)
            tvTitle.text = activity!!.getString(titleBadges[number - 1])
            tvDescription.text = activity!!.getString(badges[number - 1])
            imgTrophy.setImageResource(GetIdFromString("b"+(number).toString()))
        }
        else{
            btnGreat.text = "OK"
            tvCongrats.text = "You need to complete a mission to open this trophy"
            tvTitle.text = activity!!.getString(titleBadges[number - 1])
            tvDescription.text = activity!!.getString(badges[number - 1])
        }
        btnGreat.setOnClickListener(this)
    }
    private fun GetIdFromString(s: String): Int {
        Log.e("Debug", s)
        return activity!!.resources.getIdentifier(s, "drawable", activity!!.packageName)
    }
    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btn_great -> {
                dismiss()
            }
        }
    }
}