package cnpm.khoaluan.maxsenglishtown.model

import android.widget.Toast
import android.os.Bundle
import android.content.ContentValues.TAG
import android.speech.SpeechRecognizer
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.content.Intent
import android.app.Activity
import android.util.Log
import java.util.*


/**
 * Created by Nguyen Van Tron on 3/29/2018.
 */
class MyRecognitionListener{
    private val contextParent: Activity
    private var intent: Intent
    private var recognizer: SpeechRecognizer
    private var word : String = ""
    private var corrected : Boolean = false
    private var isEndOfSpeech : Boolean = false
    private var isWrong = false
    fun Match() : Boolean{
        return corrected
    }
    fun EngOfSpeech() : Boolean{
        return isEndOfSpeech
    }
    fun isWrong() : Boolean{
        return isWrong
    }
    constructor(contextParent: Activity, word : String){
        this.word = word
        this.contextParent = contextParent
        intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.US)
        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
                "cnpm.khoaluan.maxsenglishtown.model")
        recognizer = SpeechRecognizer
                .createSpeechRecognizer(contextParent.applicationContext)
        setRecognition()
    }

    protected fun setRecognition() {
        val listener = object : RecognitionListener {
            override fun onResults(results: Bundle) {
                val voiceResults = results
                        .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
                if (voiceResults == null) {
                    Log.e(TAG, "No voice results")
                } else {
                    Log.d(TAG, "Printing matches: ")
                    for (match in voiceResults) {
                        Log.d("Word: ", word)
                        if(match.toLowerCase().endsWith(word.toLowerCase())){
                            corrected = true
                            Log.d("Corect: ", match)
                            break
                        }
                        Log.d(TAG, match)
                    }
                }
            }

            override fun onReadyForSpeech(params: Bundle) {
                Log.d(TAG, "Ready for speech")
            }

            override fun onError(error: Int) {
                Log.d(TAG,
                        "Error listening for speech: " + error)
                isWrong = true
            }

            override fun onBeginningOfSpeech() {
                Log.d(TAG, "Speech starting")
            }

            override fun onBufferReceived(buffer: ByteArray) {
                // TODO Auto-generated method stub

            }

            override fun onEndOfSpeech() {
                // TODO Auto-generated method stub
                isEndOfSpeech = true
            }

            override fun onEvent(eventType: Int, params: Bundle) {
                // TODO Auto-generated method stub

            }

            override fun onPartialResults(partialResults: Bundle) {
                // TODO Auto-generated method stub

            }

            override fun onRmsChanged(rmsdB: Float) {
                // TODO Auto-generated method stub

            }
        }
        recognizer.setRecognitionListener(listener)
    }
    fun StartListening(){
        isEndOfSpeech = false
        corrected = false
        contextParent.runOnUiThread({
            kotlin.run {
                recognizer.startListening(intent)
            }
        })
    }
    fun SetWord(word : String){
        this.word = word
    }
    fun Destroy() {
        //Hàm này được thực hiện khi tiến trình kết thúc
        recognizer.destroy()
        Toast.makeText(contextParent, "Okie, Finished", Toast.LENGTH_SHORT).show()
    }
}