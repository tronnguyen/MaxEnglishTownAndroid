package cnpm.khoaluan.maxsenglishtown.minigame

import android.content.res.AssetFileDescriptor
import android.media.MediaPlayer
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import cnpm.khoaluan.maxsenglishtown.R
import cnpm.khoaluan.maxsenglishtown.application.ImageTargets
import cnpm.khoaluan.maxsenglishtown.global.Constants
import cnpm.khoaluan.maxsenglishtown.springy.facebook.rebound.SpringAnimationType
import cnpm.khoaluan.maxsenglishtown.springy.facebook.rebound.SpringyAnimator
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.facebook.rebound.SpringSystem
import com.vuforia.State
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import org.xmlpull.v1.XmlPullParserFactory
import java.io.IOException
import java.util.*

/**
 * Created by Nguyen Van Tron on 3/24/2018.
 */
class FindAlpha : ImageTargets()
{
    private var height : Float = 0f
    private var width : Float = 0f
    private  var level : Int = 1
    private var imageViewPicture: ImageView? = null
    var wordView : ImageView? = null
    private var btnNext: ImageButton? = null
    private var words: Dictionary<Int, String> = Hashtable()
    private val imageViewWrongWords = ArrayList<ImageView>()
    private val imageViewWords = ArrayList<ImageView>()
    private val listHide = ArrayList<Char>()
    private val listShow = ArrayList<Char>()
    private val listWrongWord = ArrayList<Char>()
    private val listExist = ArrayList<String>()
    private val tempListIndexHided = ArrayList<Int>()
    private var keyWord = ""
    private var tempCharacter = ""
    private var flagWordWasShowed = false
    private var iCountWrongWords = 0
    internal val r = Random()
    private var countTime = 0
    private var countWrong = 0
    private var soundWord : MediaPlayer? = null
    private var soundLetter : MediaPlayer? = null
    private var soundWinner : MediaPlayer? = null
    private var soundWrong : MediaPlayer? = null
    private var soundRight : MediaPlayer? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        SetUILayout(R.layout.activity_mini_game)
        super.onCreate(savedInstanceState)
        Constants.SpringSystemManager = SpringSystem.create()
        height = resources
                .displayMetrics
                .heightPixels.toFloat()
        width = resources
                .displayMetrics
                .widthPixels.toFloat()
        getmDatasetStrings().add("basic_alphabet.xml")
        setLevel(2)
        imageViewPicture = findViewById(R.id.imageview_picture)
        wordView = findViewById(R.id.word_view)
        imageViewWrongWords.add(findViewById(R.id.wrong_word_01))
        imageViewWrongWords.add(findViewById(R.id.wrong_word_02))
        imageViewWrongWords.add(findViewById(R.id.wrong_word_03))
        imageViewWrongWords.add(findViewById(R.id.wrong_word_04))
        imageViewWrongWords.add(findViewById(R.id.wrong_word_05))
        imageViewWrongWords.add(findViewById(R.id.wrong_word_06))
        imageViewWrongWords.add(findViewById(R.id.wrong_word_07))
        imageViewWrongWords.add(findViewById(R.id.wrong_word_08))
        imageViewWrongWords.add(findViewById(R.id.wrong_word_09))
        imageViewWords.add(findViewById(R.id.word_01))
        imageViewWords.add(findViewById(R.id.word_02))
        imageViewWords.add(findViewById(R.id.word_03))
        imageViewWords.add(findViewById(R.id.word_04))
        imageViewWords.add(findViewById(R.id.word_05))
        imageViewWords.add(findViewById(R.id.word_06))
        imageViewWords.add(findViewById(R.id.word_07))
        imageViewWords.add(findViewById(R.id.word_08))
        imageViewWords.add(findViewById(R.id.word_09))
        imageViewWords.add(findViewById(R.id.word_10))
        imageViewWords.add(findViewById(R.id.word_11))
        imageViewWords.add(findViewById(R.id.word_12))
        val btnBack : ImageButton = findViewById(R.id.btn_back)
        var textViewGuessTheWord : TextView = findViewById(R.id.tv_missing_letters)
        var displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val width = displayMetrics.widthPixels/displayMetrics.density
        val height = displayMetrics.heightPixels/displayMetrics.density
        when {
            width.toInt()*16 == height.toInt()*9 -> textViewGuessTheWord.textSize = 20f
            width.toInt()*4 == height.toInt()*3 -> textViewGuessTheWord.textSize = 30f
            else -> textViewGuessTheWord.textSize = 30f
        }
        for (i in imageViewWrongWords.indices) {
            Glide.with(baseContext)
                    .load(R.raw.placeholder)
                    .apply(RequestOptions.circleCropTransform())
                    .into(imageViewWrongWords[i])
        }
        btnNext = findViewById(R.id.btnNext)
        //Get data
        parseXML()
        //The firt word
        generaNextWord()
        btnNext!!.visibility = View.VISIBLE
        btnNext!!.setOnClickListener {
            generaNextWord()
            countTime++
            if(countTime == 9 && !Constants.getValue(11,"trophy12")){
                Constants.rewards[11] = true
            }
        }
        btnBack.setOnClickListener {
            onBackPressed()
        }
    }
    private fun setLevel(level : Int){
        this.level = level
    }

    override fun getClassName(): String {
        return "FindAlpha"
    }
    override fun onResume() {
        super.onResume()
        Constants.SpringSystemManager = SpringSystem.create()

    }
    private var actionDown : Boolean = false
    override fun onTouchEvent(event: MotionEvent): Boolean {
        // TODO Auto-generated method stub

        if (event.pointerCount > 1) {
            println("Multitouch detected!")
//            if(!actionDown){
//                GeneraNextWord()
//                actionDown = true
//            }
            return true
        } else {
            actionDown = false
            return super.onTouchEvent(event)
        }
    }
    //Genare Key word
    private fun generaNextWord() {
        countWrong = 0
        wordView!!.visibility = View.INVISIBLE
        wordView!!.scaleX = 1f
        wordView!!.scaleY = 1f
        listExist.clear()
        listHide.clear()
        listShow.clear()
        listWrongWord.clear()
        iCountWrongWords = 0
        tempCharacter = ""
        for (i in imageViewWrongWords.indices) {
            Glide.with(baseContext)
                    .load(R.drawable.wrongletter_placeholder)
                    .into(imageViewWrongWords[i])
        }
        val sNext = words.get(r.nextInt(words.size()) + 1)
        keyWord = sNext
        for (i in imageViewWords.indices) {
            imageViewWords[i].visibility = View.VISIBLE
            Glide.with(baseContext)
                    .load(R.drawable.missingletter_placeholder)
                    .into(imageViewWords[i])
        }
        for (i in sNext.length..11) {//edit
            imageViewWords[i].visibility = View.INVISIBLE
        }
        Glide
                .with(baseContext)
                //.load(Drawable.createFromStream(assets.open("animalImages/realImages/"+sNext+".jpg"),null))
                .load(resources.getIdentifier(sNext, "drawable", packageName))
                .into(imageViewPicture!!)
        //showToast(sNext);
        val numberHideNext = this.level
        tempListIndexHided.clear()
        var temp = -1
        run {
            var i = 0
            while (i < numberHideNext) {
                val indexCharNext =r.nextInt(sNext.length)
                if (temp != indexCharNext && !sNext[indexCharNext].toString().endsWith("5")) {
                    temp = indexCharNext
                    if(sNext.indexOf("5") > sNext.indexOf(sNext[indexCharNext]) || sNext.indexOf("4") > sNext.indexOf(sNext[indexCharNext])) {
                        tempListIndexHided.add(indexCharNext)

                    }
                    else if(sNext.indexOf("5") == 5 || sNext.indexOf("4") == 5){
                        tempListIndexHided.add(indexCharNext)
                    }
                    else{
                        if(sNext.indexOf("5") >= 0) {
                            tempListIndexHided.add(indexCharNext - sNext.indexOf("5") + -1 + 6)
                        }
                        else if(sNext.indexOf("4") >= 0){
                            tempListIndexHided.add(indexCharNext - sNext.indexOf("4") + -1 + 6)
                        }
                        else{
                            tempListIndexHided.add(indexCharNext)
                        }
                    }
                    listHide.add(sNext[indexCharNext])
                } else
                    i--
                i++
            }
        }
        var flag = false
        for (i in 0 until sNext.length) {
            if(sNext[i].toString().endsWith("5") || sNext[i].toString().endsWith("4")){
                flag = true
                if(sNext[i].toString().endsWith("4")){
                    imageViewWords[i].visibility = View.GONE
                }else{
                    Glide.with(baseContext)
                            .load(getIdFromString("wordblue" + sNext[i]))
                            .into(imageViewWords[i])
                }
                for (j in i+1 until 6) {
                    imageViewWords[j].visibility = View.GONE
                }
                var t = i + 1
                for (z in 6 until sNext.length - i - 1 + 6) {
                    if (!match(z , tempListIndexHided)) {
                        listShow.add(sNext[t])
                        imageViewWords[z].visibility = View.VISIBLE
                        Glide.with(baseContext)
                                .load(getIdFromString("wordblue" + sNext[t]))
                                .into(imageViewWords[z])
                    }
                    else{
                        imageViewWords[z].visibility = View.VISIBLE
                    }
                    t++
                }
                for (k in sNext.length - i - 1 + 6 until 12) {
                    imageViewWords[k].visibility = View.GONE
                }
                break
            }
            if (!match(i, tempListIndexHided)) {
                listShow.add(sNext[i])
                Glide.with(baseContext)
                        .load(getIdFromString("wordblue" + sNext[i]))
                        .into(imageViewWords[i])
            }

        }
        if(!flag) {
            for (i in sNext.length until 11) {
                imageViewWords[i].visibility = View.GONE
            }
        }
    }

    private fun match(index: Int, list: List<Int>): Boolean {
        for (i in list.indices) {
            if (list[i] == index)
                return true
        }
        return false
    }

    //Parse XML
    private fun parseXML() {
        val xmlPullParserFactory: XmlPullParserFactory
        try {
            xmlPullParserFactory = XmlPullParserFactory.newInstance()
            val xmlPullParser = xmlPullParserFactory.newPullParser()
            val `is` = assets.open("data_minigame.xml")
            xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false)
            xmlPullParser.setInput(`is`, null)
            words = processParsing(xmlPullParser)
        } catch (e: XmlPullParserException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    @Throws(XmlPullParserException::class, IOException::class)
    private fun processParsing(xmlPullParser: XmlPullParser): Dictionary<Int, String> {
        val words = Hashtable<Int, String>()
        var eventType = xmlPullParser.eventType
        var id = 0
        while (eventType != XmlPullParser.END_DOCUMENT) {
            var eltName: String?
            when (eventType) {
                XmlPullParser.START_TAG -> {
                    eltName = xmlPullParser.name
                    if (eltName!!.endsWith("name")) {
                        id++
                        val value = xmlPullParser.nextText()
                        words.put(id, value)
                    }
                }
            }
            eventType = xmlPullParser.next()
        }
        return words
    }

    override fun onStop() {
        super.onStop()
        soundWord?.stop()
        soundLetter?.stop()
        soundWinner?.stop()
        soundRight?.stop()
        soundWrong?.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        soundLetter?.release()
        soundWord?.release()
        soundWrong?.release()
        soundRight?.release()
        soundWinner?.release()
    }
    //getIdFromString
    private fun getIdFromString(s: String): Int? {
        Log.e("Debug", s)
        return resources.getIdentifier(s, "raw", packageName)
    }
    override fun onBackPressed() {
        if(isLoaded)
            super.onBackPressed()
    }
    override fun onVuforiaUpdate(state: State?) {
        super.onVuforiaUpdate(state)
        //Setup Spring animator
        //Detecting with camera and game processing
        if(state != null){
            for (tIdx in  0 until state.numTrackableResults) {
                val result = state.getTrackableResult(tIdx)
                val trackable = result.trackable

                if (!tempCharacter.endsWith(trackable.name.toString()) && !listExist.contains(trackable.name.toString())) {
                    tempCharacter = trackable.name.toString()
                    listExist.add(tempCharacter)

                    wordView!!.postDelayed(Runnable {
                    //Find character in list word were hided
                    flagWordWasShowed = false
                    var i = 0
                    while (i < listHide.size) {
                        if (listHide[i].toString()
                                .endsWith(tempCharacter
                                        .toLowerCase()[tempCharacter.length - 1])) {
                            listShow.add(listHide[i])
                            for (j in 0 until tempListIndexHided.size){
                                if (((i>-1)&&(tempListIndexHided[j] + keyWord.indexOf("5") + 1 - 6 > -1)&&(keyWord.indexOf("5") >=0) &&
                                                ((keyWord[tempListIndexHided[j] + keyWord.indexOf("5") + 1 - 6])
                                        == listHide[i])) ||
                                        ((i>-1)&&(tempListIndexHided[j] + keyWord.indexOf("4") + 1 - 6 > -1)&&((keyWord.indexOf("4") >=0) &&
                                                (keyWord[tempListIndexHided[j] + keyWord.indexOf("4") + 1 - 6])
                                                == listHide[i]))) {
                                    soundRight = MediaPlayer()
                                    var afd = assets.openFd("sounds/effects/correct_answer.mp3")
                                    soundRight?.setDataSource(afd.fileDescriptor,afd.startOffset, afd.length)
                                    afd.close()
                                    soundRight?.prepare()
                                    soundRight?.start()
                                    val count = tempListIndexHided[j]
                                    val spring2 = SpringyAnimator(SpringAnimationType.CUSTOMTYPE,
                                            width/2, 0f, height, 0f, Constants.SpringSystemManager)
                                    wordView!!.visibility = View.INVISIBLE
                                    wordView!!.postDelayed(Runnable {
                                        spring2.startCustomSpring(wordView)
                                        wordView!!.visibility = View.VISIBLE
                                    }, (500).toLong())
                                        Glide.with(baseContext)
                                                .load(getIdFromString("wordorange" +
                                                        tempCharacter
                                                                .toLowerCase()
                                                                .substring(tempCharacter.length - 1)))
                                                .into(wordView!!)
                                        imageViewWords.get(count).visibility = View.INVISIBLE

                                    val spring = SpringyAnimator(SpringAnimationType.CUSTOMTYPEEND,
                                            width/2 - 200, 0f, -height/2, 0f, Constants.SpringSystemManager)
                                    imageViewWords[count].postDelayed(Runnable {
                                        spring.startCustomSpring(imageViewWords[count])
                                        imageViewWords.get(count).visibility = View.VISIBLE
                                        wordView!!.visibility = View.INVISIBLE
                                    }, (1500).toLong())
                                        Glide.with(baseContext)
                                                .load(getIdFromString("wordorange" +
                                                        tempCharacter
                                                                .toLowerCase()
                                                                .substring(tempCharacter.length - 1)))
                                                .into(imageViewWords[tempListIndexHided[j]])
                                        //tempListIndexHided.remove(j)
                                    listHide.removeAt(i)
                                    i--
                                    flagWordWasShowed = true
                                }
                                else if (i >= 0 && tempListIndexHided[j] < keyWord.length && keyWord[tempListIndexHided[j]]== listHide[i]) {
                                    soundRight = MediaPlayer()
                                    var afd = assets.openFd("sounds/effects/correct_answer.mp3")
                                    soundRight?.setDataSource(afd.fileDescriptor,afd.startOffset, afd.length)
                                    afd.close()
                                    soundRight?.prepare()
                                    soundRight?.start()
                                    val count = tempListIndexHided[j]
                                    val spring2 = SpringyAnimator(SpringAnimationType.CUSTOMTYPE,
                                            width/2, 0f, height, 0f, Constants.SpringSystemManager)
                                    wordView!!.visibility = View.INVISIBLE
                                    wordView!!.postDelayed(Runnable {
                                        spring2.startCustomSpring(wordView)
                                        wordView!!.visibility = View.VISIBLE
                                    }, (500).toLong())
                                        Glide.with(baseContext)
                                                .load(getIdFromString("wordorange" +
                                                        tempCharacter
                                                                .toLowerCase()
                                                                .substring(tempCharacter.length - 1)))
                                                .into(wordView!!)
                                        imageViewWords.get(count).visibility = View.INVISIBLE
                                    val spring = SpringyAnimator(SpringAnimationType.CUSTOMTYPEEND,
                                            width/2 - 200, 0f, -height/2, 0f, Constants.SpringSystemManager)
                                    imageViewWords[count].postDelayed(Runnable {
                                        spring.startCustomSpring(imageViewWords[count])
                                        imageViewWords.get(count).visibility = View.VISIBLE
                                        wordView!!.visibility = View.INVISIBLE
                                    }, (1500).toLong())
                                        Glide.with(baseContext)
                                                .load(getIdFromString("wordorange" +
                                                        tempCharacter
                                                                .toLowerCase()
                                                                .substring(tempCharacter.length - 1)))
                                                .into(imageViewWords[tempListIndexHided[j]])
                                        //tempListIndexHided.remove(j)
                                    listHide.removeAt(i)
                                    i--
                                    flagWordWasShowed = true
                                }
                            }
                        }
                        else if(iCountWrongWords < 9 && i == listHide.size - 1 && !flagWordWasShowed){
                            //Add wrong word to wronglist
                            soundWord = MediaPlayer()
                            var afd = assets.openFd("sounds/effects/error_sound.mp3")
                            soundWord?.setDataSource(afd.fileDescriptor,afd.startOffset, afd.length)
                            afd.close()
                            soundWord?.prepare()
                            soundWord?.start()
                            listWrongWord.add(tempCharacter
                                    .toLowerCase()
                                    [(tempCharacter.length - 1)])
                            val count = iCountWrongWords
                            //Move on center
                            val spring4 = SpringyAnimator(SpringAnimationType.CUSTOMTYPE,
                                    width/2, 0f, height, 0f, Constants.SpringSystemManager)
                            wordView!!.visibility = View.INVISIBLE
                            wordView!!.postDelayed(Runnable {
                                spring4.startCustomSpring(wordView)
                                wordView!!.visibility = View.VISIBLE
                            }, (500).toLong())
                                Glide.with(baseContext)
                                        .load(getIdFromString("wordred" +
                                                tempCharacter
                                                        .toLowerCase()
                                                        .substring(tempCharacter.length - 1)))
                                        .into(wordView!!)
                            //Back to top
                            val spring3 = SpringyAnimator(SpringAnimationType.TRANSLATEY,
                                    0f, 0f, height/2, 0f, Constants.SpringSystemManager)

                            imageViewWrongWords[count].visibility = View.INVISIBLE
                            imageViewWrongWords[count].postDelayed(Runnable {
                                spring3.startSpring(imageViewWrongWords[count])
                                imageViewWrongWords[count].visibility = View.VISIBLE
                                wordView!!.visibility = View.INVISIBLE
                                countWrong++
                                if(countWrong in 3..6){
                                    soundWord = MediaPlayer()
                                    var afd : AssetFileDescriptor? = null
                                    afd = try{
                                        assets.openFd("sounds/examples/ex_"+
                                                keyWord.replace("4","").replace("5","")+".mp3")
                                    } catch (e : Exception ){
                                        assets.openFd("sounds/animals/"+
                                                keyWord.replace("4","").replace("5","")+".mp3")
                                    }
                                    soundWord?.setDataSource(afd?.fileDescriptor,afd!!.startOffset, afd.length)
                                    afd?.close()
                                    soundWord?.prepare()
                                    soundWord?.start()
                                }
                                if(countWrong in 7..9){
                                    soundLetter = MediaPlayer()
                                    var afd = assets.openFd("sounds/alphabets/"+ listHide[0].toString()
                                                    +".mp3")
                                    soundLetter?.setDataSource(afd.fileDescriptor,afd.startOffset, afd.length)
                                    afd.close()
                                    soundLetter?.prepare()
                                    soundLetter?.start()
                                    if(iCountWrongWords == 9){
                                        listHide.clear()
                                    }
                                }
                            }, (1500).toLong())
                                Glide.with(baseContext).load(getIdFromString("wordred" +
                                        tempCharacter
                                                .toLowerCase()
                                                .substring(tempCharacter.length - 1)))
                                        .into(imageViewWrongWords[iCountWrongWords])
                                iCountWrongWords++
                            if(iCountWrongWords == 9){
                                for (j in 0 until listHide.size){
                                    if (((i>-1)&&(tempListIndexHided[j] + keyWord.indexOf("5") + 1 - 6 > -1)&&(keyWord.indexOf("5") >=0) &&
                                                    ((keyWord[tempListIndexHided[j] + keyWord.indexOf("5") + 1 - 6])
                                                            == listHide[j])) ||
                                            ((i>-1)&&(tempListIndexHided[j] + keyWord.indexOf("4") + 1 - 6 > -1)&&((keyWord.indexOf("4") >=0) &&
                                                    (keyWord[tempListIndexHided[j] + keyWord.indexOf("4") + 1 - 6])
                                                    == listHide[j]))) {
                                        soundRight = MediaPlayer()
                                        var afd = assets.openFd("sounds/effects/correct_answer.mp3")
                                        soundRight?.setDataSource(afd.fileDescriptor,afd.startOffset, afd.length)
                                        afd.close()
                                        soundRight?.prepare()
                                        soundRight?.start()
                                        val count = tempListIndexHided[j]
//                                    val spring2 = SpringyAnimator(SpringAnimationType.CUSTOMTYPE,
//                                            width/2, 0f, height, 0f, Constants.SpringSystemManager)
//                                    wordView!!.visibility = View.INVISIBLE
//                                    wordView!!.postDelayed(Runnable {
//                                        spring2.startCustomSpring(wordView)
//                                        wordView!!.visibility = View.VISIBLE
//                                    }, (2000).toLong())
//                                    Glide.with(baseContext)
//                                            .load(getIdFromString("wordorange" +
//                                                    listHide[j]))
//                                            .into(wordView!!)
                                        imageViewWords.get(count).visibility = View.INVISIBLE

                                        val spring = SpringyAnimator(SpringAnimationType.CUSTOMTYPEEND,
                                                width/2 - 200, 0f, -height/2, 0f, Constants.SpringSystemManager)
                                        imageViewWords[count].postDelayed(Runnable {
                                            spring.startCustomSpring(imageViewWords[count])
                                            imageViewWords.get(count).visibility = View.VISIBLE
                                            wordView!!.visibility = View.INVISIBLE
                                        }, (1500).toLong())
                                        Glide.with(baseContext)
                                                .load(getIdFromString("wordorange" +
                                                        listHide[j]))
                                                .into(imageViewWords[tempListIndexHided[j]])
                                        //tempListIndexHided.remove(j)
                                        //listHide.removeAt(i)
                                        flagWordWasShowed = true
                                    }
                                    else if (i >= 0 && tempListIndexHided[j] < keyWord.length && keyWord[tempListIndexHided[j]]== listHide[j]) {
                                        soundRight = MediaPlayer()
                                        var afd = assets.openFd("sounds/effects/correct_answer.mp3")
                                        soundRight?.setDataSource(afd.fileDescriptor,afd.startOffset, afd.length)
                                        afd.close()
                                        soundRight?.prepare()
                                        soundRight?.start()
                                        val count = tempListIndexHided[j]
                                        val spring2 = SpringyAnimator(SpringAnimationType.CUSTOMTYPE,
                                                width/2, 0f, height, 0f, Constants.SpringSystemManager)
//                                    wordView!!.visibility = View.INVISIBLE
//                                    wordView!!.postDelayed(Runnable {
//                                        spring2.startCustomSpring(wordView)
//                                        wordView!!.visibility = View.VISIBLE
//                                    }, (500).toLong())
//                                    Glide.with(baseContext)
//                                            .load(getIdFromString("wordorange" +
//                                                    listHide[j]))
//                                            .into(wordView!!)
                                        imageViewWords.get(count).visibility = View.INVISIBLE
                                        val spring = SpringyAnimator(SpringAnimationType.CUSTOMTYPEEND,
                                                width/2 - 200, 0f, -height/2, 0f, Constants.SpringSystemManager)
                                        imageViewWords[count].postDelayed(Runnable {
                                            spring.startCustomSpring(imageViewWords[count])
                                            imageViewWords.get(count).visibility = View.VISIBLE
                                            wordView!!.visibility = View.INVISIBLE
                                        }, (1500).toLong())
                                        Glide.with(baseContext)
                                                .load(getIdFromString("wordorange" +
                                                        listHide[j]))
                                                .into(imageViewWords[tempListIndexHided[j]])
                                        //tempListIndexHided.remove(j)
                                        flagWordWasShowed = true
                                    }
                                }
                            }
                                break
                        }
                        i++
                    }
                    if(listHide.size <= 0)
                    {
                        btnNext?.postDelayed(Runnable {
                            soundWinner = MediaPlayer()
                            var afd = assets.openFd("sounds/effects/game_winner.mp3")
                            soundWinner?.setDataSource(afd.fileDescriptor,afd.startOffset, afd.length)
                            afd.close()
                            soundWinner?.prepare()
                            soundWinner?.start()
                            generaNextWord()
                            countTime++
                            if(countTime == 9 && !Constants.getValue(11,"trophy12")){
                                Constants.rewards[11] = true
                            }
                        },4000L)

                    }
                    },0L)
                }

            }
        }
    }
}