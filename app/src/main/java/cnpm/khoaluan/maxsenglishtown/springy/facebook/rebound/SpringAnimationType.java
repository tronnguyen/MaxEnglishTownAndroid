package cnpm.khoaluan.maxsenglishtown.springy.facebook.rebound;

/**
 * Created by salman on 21/11/16.
 */

public enum SpringAnimationType {
    TRANSLATEX,
    TRANSLATEY,
    CUSTOMTYPE,
    CUSTOMTYPEEND,
    ROTATEX,
    ROTATEY,
    SCALEXY,
    SCALEX,
    SCALEY,
    ALPHA,
    ROTATION
}
