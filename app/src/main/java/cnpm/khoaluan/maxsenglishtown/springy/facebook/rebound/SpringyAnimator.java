package cnpm.khoaluan.maxsenglishtown.springy.facebook.rebound;

import android.view.View;
import android.widget.ImageView;

import com.facebook.rebound.SimpleSpringListener;
import com.facebook.rebound.Spring;
import com.facebook.rebound.SpringConfig;
import com.facebook.rebound.SpringSystem;
import com.facebook.rebound.SpringUtil;


/**
 * Created by salman on 17/11/16.
 */

public class SpringyAnimator {
    private float startValue, endValue;
    private  static final float DEFAULT_TENSION = 40;
    private  static final float DEFAULT_FRACTION = 7;
    private double tension,fraction;
    private SpringSystem springSystem;
    private SpringAnimationType animationType;
    private SpringyListener springAnimatorListener;
    private int delay = 0;


    public void SetTranslateXY(float translateX, float translateY) {
        this.translateX = translateX;
        this.translateY = translateY;
    }
    public void SetEndTranslateXY(float translateX, float translateY) {
        this.endTranslateX = translateX;
        this.endTranslateY = translateY;
    }
    private float translateX;
    private float translateY;
    private float endTranslateX;
    private float endTranslateY;

    /**
     * Constructor for with Animation Type + Spring config + animation Values
     * * @param springConfig config class for the spring
     * @param  type SpringyAnimationType instance for animation type
     * @param  tension Spring tension for animation type
     * @param  fraction Spring fraction value for animation
     * @param  startValue where animation start from
     * @param  endValue where animation ends to
     * **/


    public SpringyAnimator(SpringAnimationType type, double tension,
                           double fraction, float startValue, float endValue) {
        this.tension = tension;
        this.fraction = fraction;
        this.startValue = startValue;
        this.endValue = endValue;
        springSystem = SpringSystem.create();
        animationType = type;

    }
    public SpringyAnimator(SpringAnimationType type, float startValueX,
                           float endValueX, float startValue, float endValue,SpringSystem springSystemCreate) {
        this.tension = 10;
        this.fraction = 5;
        this.startValue = startValue;
        this.endValue = endValue;
        this.translateX = startValueX;
        this.endTranslateX = endValueX;
        springSystem = springSystemCreate;
        animationType = type;

    }


    /**
     * Constructor for with Animation Type + default config for spring + animation Values
     * * @param springConfig config class for the spring
     * @param  type SpringyAnimationType instance for animation type
     * @param  startValue where animation start from
     * @param  endValue where animation ends to
     * **/
      public SpringyAnimator(SpringAnimationType type, float startValue,
                             float endValue) {
        this.tension = DEFAULT_TENSION;
        this.fraction = DEFAULT_FRACTION;
        this.startValue = startValue;
        this.endValue = endValue;
        springSystem = SpringSystem.create();
        animationType = type;

    }

    public void SetAnimationType(SpringAnimationType type){
        animationType = type;
    }


    /**
     * @param  delay   int value  for SpringyAnimation delay each item if we have multiple items in
     *                 animation  raw.
     * **/
    public void setDelay(int delay) {
        this.delay = delay;
    }

    //Custom
    public void startCustomSpring(final ImageView view) {
        setInitValue(view);

        Runnable startAnimation = new Runnable() {
            @Override
            public void run() {
                Spring spring = springSystem.createSpring();
                spring.setSpringConfig(SpringConfig.fromOrigamiTensionAndFriction(tension, fraction));
                spring.addListener(new SimpleSpringListener() {
                    @Override
                    public void onSpringUpdate(Spring spring) {
                        view.setVisibility(View.VISIBLE);
                        final float value = (float) SpringUtil.mapValueFromRangeToRange(spring.getCurrentValue(), 0, 1, startValue, endValue);
                        final float valueX = (float) SpringUtil.mapValueFromRangeToRange(spring.getCurrentValue(), 0, 1, translateY, endTranslateX);
                        final float valueEnd = (float) SpringUtil.mapValueFromRangeToRange(spring.getCurrentValue(), 0, 1, translateX, endTranslateX);

                        final float vScaleBegin = (float) SpringUtil.mapValueFromRangeToRange(spring.getCurrentValue(), 0, 1, 1f, 3f);
                        final float vScaleEnd = (float) SpringUtil.mapValueFromRangeToRange(spring.getCurrentValue(), 0, 1, 3f, 1f);
                        switch (animationType) {
                            case CUSTOMTYPE:
                                view.setTranslationY(value);
                                view.setTranslationX(valueX);
                                view.setScaleX(vScaleBegin);
                                view.setScaleY(vScaleBegin);
                                break;
                            case CUSTOMTYPEEND:
                                view.setTranslationX(valueEnd);
                                view.setTranslationY(value);
                                view.setScaleX(vScaleEnd);
                                view.setScaleY(vScaleEnd);
                                break;
                            case TRANSLATEY:
                                view.setTranslationX(valueEnd);
                                view.setTranslationY(value);
                                break;
                            case TRANSLATEX:
                                view.setTranslationX(value);
                                break;
                            case ALPHA:
                                view.setAlpha(value);
                                break;
                            case SCALEY:
                                view.setScaleY(value);
                                break;
                            case SCALEX:
                                view.setScaleX(value);
                                break;
                            case SCALEXY:
                                view.setScaleY(value);
                                view.setScaleX(value);
                                break;
                            case ROTATEY:
                                view.setRotationY(value);
                                break;
                            case ROTATEX:
                                view.setRotationX(value);
                                break;
                            case ROTATION:
                                view.setRotation(value);
                                break;
                        }
                    }

                    @Override
                    public void onSpringAtRest(Spring spring) {
                        if (springAnimatorListener != null){
                            springAnimatorListener.onSpringStop();
                        }
                    }

                    @Override
                    public void onSpringActivate(Spring spring) {
                        if (springAnimatorListener != null){
                            springAnimatorListener.onSpringStart();
                        }
                    }

                });
                spring.setEndValue(1);
            }
        };
        view.postDelayed(startAnimation, delay);

    }
    public void startSpring(final ImageView view) {
        setInitValue(view);
        Runnable startAnimation = new Runnable() {
            @Override
            public void run() {
                Spring spring = springSystem.createSpring();
                spring.setSpringConfig(SpringConfig.fromOrigamiTensionAndFriction(tension, fraction));
                spring.addListener(new SimpleSpringListener() {
                    @Override
                    public void onSpringUpdate(Spring spring) {
                        view.setVisibility(View.VISIBLE);
                        final float value = (float) SpringUtil.mapValueFromRangeToRange(spring.getCurrentValue(), 0, 1, startValue, endValue);
                        switch (animationType) {
                            case TRANSLATEY:
                                view.setTranslationY(value);
                                break;
                            case TRANSLATEX:
                                view.setTranslationX(value);
                                break;
                            case ALPHA:
                                view.setAlpha(value);
                                break;
                            case SCALEY:
                                view.setScaleY(value);
                                break;
                            case SCALEX:
                                view.setScaleX(value);
                                break;
                            case SCALEXY:
                                view.setScaleY(value);
                                view.setScaleX(value);
                                break;
                            case ROTATEY:
                                view.setRotationY(value);
                                break;
                            case ROTATEX:
                                view.setRotationX(value);
                                break;
                            case ROTATION:
                                view.setRotation(value);
                                break;
                        }
                    }

                    @Override
                    public void onSpringAtRest(Spring spring) {
                        if (springAnimatorListener != null){
                            springAnimatorListener.onSpringStop();
                        }
                    }

                    @Override
                    public void onSpringActivate(Spring spring) {
                        if (springAnimatorListener != null){
                            springAnimatorListener.onSpringStart();
                        }
                    }

                });
                spring.setEndValue(1);
            }
        };
        view.postDelayed(startAnimation, delay);

    }

    /**
     * @param  view  instance for  set pre animation value
     * **/
    private void setInitValue(View view) {
        switch (animationType) {
            case CUSTOMTYPE:
                view.setTranslationY(startValue);
                view.setTranslationX(translateX);
                view.setScaleY(3f);
                view.setScaleX(3f);
                break;
            case CUSTOMTYPEEND:
                view.setTranslationY(startValue);
                view.setTranslationX(translateX);
                break;
            case TRANSLATEY:
                view.setTranslationY(startValue);
                view.setTranslationX(translateX);
                break;
            case TRANSLATEX:
                view.setTranslationX(startValue);
                break;
            case ALPHA:
                view.setAlpha(startValue);
                break;
            case SCALEY:
                view.setScaleY(startValue);
                break;
            case SCALEX:
                view.setScaleX(startValue);
                break;
            case SCALEXY:
                view.setScaleY(startValue);
                view.setScaleX(startValue);
                break;
            case ROTATEY:
                view.setRotationY(startValue);
                break;
            case ROTATEX:
                view.setRotationX(startValue);
                break;
            case ROTATION:
                view.setRotation(startValue);
                break;
        }
    }

/*
* Springy Listener to track the Spring
* */
    public void setSpringyListener(SpringyListener springyListener) {
        this.springAnimatorListener = springyListener;
    }


}
