package cnpm.khoaluan.maxsenglishtown.application;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.PlaybackParams;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.vuforia.CameraDevice;
import com.vuforia.DataSet;
import com.vuforia.Image;
import com.vuforia.ObjectTracker;
import com.vuforia.STORAGE_TYPE;
import com.vuforia.State;
import com.vuforia.Trackable;
import com.vuforia.Tracker;
import com.vuforia.TrackerManager;
import com.vuforia.Vuforia;

import java.util.ArrayList;

import cnpm.khoaluan.maxsenglishtown.R;

import static com.vuforia.PIXEL_FORMAT.RGB888;

/**
 * Created by Nguyen Van Tron on 2/6/2018.
 */

public class ImageTargets extends Activity implements ApplicationControl
{
    private static final String LOGTAG = "ImageTargets";

    ApplicationSession vuforiaAppSession;
    // Our OpenGL view:
    private ApplicationGLView mGlView;
    // Our renderer:
    private ImageTargetRenderer mRenderer;
    private DataSet mCurrentDataset;
    private boolean isLoaded = false;
    private int mCurrentDatasetSelectionIndex = 0;
    private ArrayList<String> mDatasetStrings = new ArrayList<String>();
    private GestureDetector mGestureDetector;
    private boolean mSwitchDatasetAsap = false;
    private boolean mContAutofocus = true;
    private boolean mExtendedTracking = false;
    private RelativeLayout mUILayout;
    private LinearLayout mUILinearLayout;
    LoadingDialogHandler loadingDialogHandler = new LoadingDialogHandler(this);

    // Alert Dialog used to display SDK errors
    private AlertDialog mErrorDialog;

    boolean mIsDroidDevice = false;
    // Called when the activity first starts or the user navigates back to an
    // activity.
    //Todo add id for background
    private int drawableRes = -1;
    //Sound
    private MediaPlayer mediaPlayer;
    private Boolean isPlay = false;
    public void SetUILayout(int id ) {
        if(getClass().getSimpleName().endsWith("CafeActivity") ||
                getClass().getSimpleName().endsWith("LibraryInputActivity")) {
            mUILinearLayout = (LinearLayout) View.inflate(this, id,
                    null);
        }
        else{
            mUILayout = (RelativeLayout) View.inflate(this, id,
                    null);
        }
    }
    public void setmCurrentDatasetSelectionIndex(int mCurrentDatasetSelectionIndex) {
        this.mCurrentDatasetSelectionIndex = mCurrentDatasetSelectionIndex;
    }
    public void setDrawableRes(int id){
        drawableRes = id;
    }
    public ArrayList<String> getmDatasetStrings() {
        return mDatasetStrings;
    }
    public LinearLayout getmUILinearLayout() {
        return mUILinearLayout;
    }
    public DataSet getmCurrentDataset() {
        return mCurrentDataset;
    }
    public boolean isLoaded(){return  isLoaded;}
    public boolean isPlay(){
        return isPlay;
    }
    public void setPlay(boolean play){
        isPlay = play;
    }
    public String getClassName(){
        return "ImageTargets";
    }
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        //drawableRes = R.drawable.background;
        Log.d(LOGTAG, "onCreate");
        super.onCreate(savedInstanceState);
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        // Set fullscreen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if(!(getClassName().endsWith("FindAlpha") ||
                getClassName().endsWith("CafeActivity") ||
                getClassName().endsWith("LibraryInputActivity"))
        ){
            mediaPlayer = MediaPlayer.create(this, R.raw.help);
        }
        // Hide title.
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        vuforiaAppSession = new ApplicationSession(this);

        startLoadingAnimation();
        //mDatasetStrings.add("data_alphabet.xml");


        vuforiaAppSession
                .initAR(this, ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mGestureDetector = new GestureDetector(this, new GestureListener());


        mIsDroidDevice = android.os.Build.MODEL.toLowerCase().startsWith(
                "droid");
    }
    // Process Single Tap event to trigger autofocus
    private class GestureListener extends
            GestureDetector.SimpleOnGestureListener
    {
        // Used to set autofocus one second after a manual focus is triggered
        private final Handler autofocusHandler = new Handler();


        @Override
        public boolean onDown(MotionEvent e)
        {
            return true;
        }


        @Override
        public boolean onSingleTapUp(MotionEvent e)
        {
            boolean result = CameraDevice.getInstance().setFocusMode(
                    CameraDevice.FOCUS_MODE.FOCUS_MODE_TRIGGERAUTO);
            if (!result)
                Log.e("SingleTapUp", "Unable to trigger focus");

            // Generates a Handler to trigger continuous auto-focus
            // after 1 second
            autofocusHandler.postDelayed(new Runnable()
            {
                public void run()
                {
                    if (mContAutofocus)
                    {
                        final boolean autofocusResult = CameraDevice.getInstance().setFocusMode(
                                CameraDevice.FOCUS_MODE.FOCUS_MODE_CONTINUOUSAUTO);

                        if (!autofocusResult)
                            Log.e("SingleTapUp", "Unable to re-enable continuous auto-focus");
                    }
                }
            }, 1000L);

            return true;
        }
    }



    // Called when the activity will start interacting with the user.
    @Override
    protected void onResume()
    {
        Log.d(LOGTAG, "onResume");
        super.onResume();

        isLoaded = false;
        showProgressIndicator(false);//Edit by Tron true to false

        // This is needed for some Droid devices to force portrait
        if (mIsDroidDevice)
        {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        vuforiaAppSession.onResume();
    }


    // Callback for configuration changes the activity handles itself
    @Override
    public void onConfigurationChanged(Configuration config)
    {
        Log.d(LOGTAG, "onConfigurationChanged");
        super.onConfigurationChanged(config);

        vuforiaAppSession.onConfigurationChanged();
    }


    // Called when the system is about to start resuming a previous activity.
    public void onNext(){
        try
        {
            vuforiaAppSession.stopAR();
        } catch (ApplicationException e)
        {
            Log.e(LOGTAG, e.getString());
        }
        //Stop camera
        CameraDevice.getInstance().stop();
        CameraDevice.getInstance().deinit();
        System.gc();
    }
    @Override
    protected void onPause()
    {
        if(!(getClassName().endsWith("FindAlpha") ||
                getClassName().endsWith("CafeActivity") ||
                getClassName().endsWith("LibraryInputActivity"))
        ){
            if (mGlView != null) {
                mGlView.setVisibility(View.INVISIBLE);
                mGlView.onPause();
            }
        }

        Log.d(LOGTAG, "onPause");
        super.onPause();
        try
        {
            vuforiaAppSession.pauseAR();
        } catch (ApplicationException e)
        {
            Log.e(LOGTAG, e.getString());
        }
        //Stop camera
        CameraDevice.getInstance().stop();
        CameraDevice.getInstance().deinit();
        if(!(getClassName().endsWith("FindAlpha") ||
                getClassName().endsWith("CafeActivity") ||
                getClassName().endsWith("LibraryInputActivity"))
        ){
            try
            {
                mediaPlayer.stop();
            } catch (IllegalStateException e)
            {
                Log.e(LOGTAG,e.toString());
            }

        }
    }


    // The final call you receive before your activity is destroyed.
    @Override
    protected void onDestroy()
    {
        Log.d(LOGTAG, "onDestroy");
        super.onDestroy();

        try
        {
            vuforiaAppSession.stopAR();
        } catch (ApplicationException e)
        {
            Log.e(LOGTAG, e.getString());
        }
        //Stop camera
        CameraDevice.getInstance().stop();
        CameraDevice.getInstance().deinit();
        if(!(getClassName().endsWith("FindAlpha") ||
                getClassName().endsWith("CafeActivity") ||
                getClassName().endsWith("LibraryInputActivity"))
        ){
            try
            {
                mediaPlayer.release();
            } catch (IllegalStateException e)
            {
                Log.e(LOGTAG,e.toString());
            }
        }
        System.gc();
    }


    // Initializes AR application components.
    private void initApplicationAR()
    {
        // Create OpenGL ES view:
        int depthSize = 16;
        int stencilSize = 0;
        boolean translucent = Vuforia.requiresAlpha();
        if(!(getClassName().endsWith("FindAlpha") ||
                getClassName().endsWith("CafeActivity") ||
                getClassName().endsWith("LibraryInputActivity"))
        ){
        mGlView = new ApplicationGLView(this);
        mGlView.init(translucent, depthSize, stencilSize);

        mRenderer = new ImageTargetRenderer(this, vuforiaAppSession);
        //mRenderer.setTextures(mTextures);
            mGlView.setRenderer(mRenderer);
        }
    }


    private void startLoadingAnimation()
    {
//        mUILayout = (RelativeLayout) View.inflate(this, R.layout.activity_minigame,
//                null);

        if(getClassName().endsWith("CafeActivity")||
                getClassName().endsWith("LibraryInputActivity")) {
            mUILinearLayout.setVisibility(View.VISIBLE);
            mUILinearLayout.setBackgroundColor(Color.WHITE);
            // Gets a reference to the loading dialog
            loadingDialogHandler.mLoadingDialogContainer = mUILinearLayout
                    .findViewById(R.id.loading_indicator);

            // Shows the loading indicator at start
            loadingDialogHandler
                    .sendEmptyMessage(LoadingDialogHandler.SHOW_LOADING_DIALOG);

            // Adds the inflated layout to the view
            addContentView(mUILinearLayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
        }else{
            mUILayout.setVisibility(View.VISIBLE);
            mUILayout.setBackgroundColor(Color.WHITE);
            // Gets a reference to the loading dialog
            loadingDialogHandler.mLoadingDialogContainer = mUILayout
                    .findViewById(R.id.loading_indicator);

            // Shows the loading indicator at start
            loadingDialogHandler
                    .sendEmptyMessage(LoadingDialogHandler.SHOW_LOADING_DIALOG);

            // Adds the inflated layout to the view
            addContentView(mUILayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
        }




    }

    // Methods to load and destroy tracking data.
    @Override
    public boolean doLoadTrackersData()
    {
        TrackerManager tManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) tManager
                .getTracker(ObjectTracker.getClassType());
        if (objectTracker == null)
            return false;

        if (mCurrentDataset == null)
            mCurrentDataset = objectTracker.createDataSet();

        if (mCurrentDataset == null)
            return false;

        if (!mCurrentDataset.load(
                mDatasetStrings.get(mCurrentDatasetSelectionIndex),
                STORAGE_TYPE.STORAGE_APPRESOURCE))
            return false;

        if (!objectTracker.activateDataSet(mCurrentDataset))
            return false;

        int numTrackables = mCurrentDataset.getNumTrackables();
        for (int count = 0; count < numTrackables; count++)
        {
            Trackable trackable = mCurrentDataset.getTrackable(count);
            if(isExtendedTrackingActive())
            {
                trackable.startExtendedTracking();
            }

            String name = "Current Dataset : " + trackable.getName();
            trackable.setUserData(name);
            Log.d(LOGTAG, "UserData:Set the following user data "
                    + (String) trackable.getUserData());
        }

        return true;
    }

    private void showToast(String text)
    {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
    @Override
    public boolean doUnloadTrackersData()
    {
        // Indicate if the trackers were unloaded correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        ObjectTracker objectTracker = (ObjectTracker) tManager
                .getTracker(ObjectTracker.getClassType());
        if (objectTracker == null)
            return false;

        if (mCurrentDataset != null && mCurrentDataset.isActive())
        {
            if (objectTracker.getActiveDataSet(0).equals(mCurrentDataset)
                    && !objectTracker.deactivateDataSet(mCurrentDataset))
            {
                result = false;
            } else if (!objectTracker.destroyDataSet(mCurrentDataset))
            {
                result = false;
            }

            mCurrentDataset = null;
        }

        return result;
    }

    @Override
    public void onVuforiaResumed()
    {
        if (mGlView != null)
        {
            mGlView.setVisibility(View.VISIBLE);
            mGlView.onResume();
        }
    }

    @Override
    public void onVuforiaStarted()
    {
        if(!(getClassName().endsWith("FindAlpha") ||
                getClassName().endsWith("CafeActivity") ||
                getClassName().endsWith("LibraryInputActivity"))
        ){
            mRenderer.updateConfiguration();
        }

        if (mContAutofocus)
        {
            // Set camera focus mode
            if(!CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_CONTINUOUSAUTO))
            {
                // If continuous autofocus mode fails, attempt to set to a different mode
                if(!CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_TRIGGERAUTO))
                {
                    CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_NORMAL);
                }

            }
            else
            {
            }
        }
        else
        {
        }
        showProgressIndicator(false);
        isLoaded = true;
    }

    public void showProgressIndicator(boolean show)
    {
        if (loadingDialogHandler != null)
        {
            if (show)
            {
                isPlay = false;
                loadingDialogHandler
                        .sendEmptyMessage(LoadingDialogHandler.SHOW_LOADING_DIALOG);
            }
            else
            {
                if(!(getClassName().endsWith("FindAlpha") ||
                        getClassName().endsWith("CafeActivity") ||
                        getClassName().endsWith("LibraryInputActivity"))
                ){
                    //Start media
                    mediaPlayer.start();
                    isPlay = true;
                }
                loadingDialogHandler
                        .sendEmptyMessage(LoadingDialogHandler.HIDE_LOADING_DIALOG);
            }
        }
    }

    @Override
    public void onInitARDone(ApplicationException exception)
    {

        if (exception == null)
        {
            initApplicationAR();
            if(!(getClassName().endsWith("FindAlpha") ||
                    getClassName().endsWith("CafeActivity") ||
                    getClassName().endsWith("LibraryInputActivity"))
            ){
                mRenderer.setActive(true);
                // Now add the GL surface view. It is important
                // that the OpenGL ES surface view gets added
                // BEFORE the camera is started and video
                // background is configured.
                addContentView(mGlView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
            }
            if(getClassName().endsWith("CafeActivity")||
                    getClassName().endsWith("LibraryInputActivity")) {
                // Sets the UILayout to be drawn in front of the camera
                mUILinearLayout.bringToFront();

                // Sets the layout background to transparent
                mUILinearLayout.setBackgroundColor(Color.TRANSPARENT);
                if(getClass().getSimpleName().endsWith("LibraryInputActivity")) {
                    mUILinearLayout.setBackgroundResource(R.drawable.background_library_chose);
                }
            }
            else{
                // Sets the UILayout to be drawn in front of the camera
                mUILayout.bringToFront();

                // Sets the layout background to transparent
                mUILayout.setBackgroundColor(Color.TRANSPARENT);
                //mUILayout.setBackgroundResource(drawableRes);
            }
            if(checkCameraFront(getBaseContext())) {
                vuforiaAppSession.startAR(CameraDevice.CAMERA_DIRECTION.CAMERA_DIRECTION_FRONT);
            }
            else{
                vuforiaAppSession.startAR(CameraDevice.CAMERA_DIRECTION.CAMERA_DIRECTION_DEFAULT);
            }

        } else
        {
            Log.e(LOGTAG, exception.getString());
            showInitializationErrorMessage(exception.getString());
        }
    }
    public static boolean checkCameraFront(Context context) {
        if(context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT)) {
            return true;
        } else {
            return false;
        }
    }

    // Shows initialization error messages as System dialogs
    public void showInitializationErrorMessage(String message)
    {
        final String errorMessage = message;
        runOnUiThread(new Runnable()
        {
            public void run()
            {
                if (mErrorDialog != null)
                {
                    mErrorDialog.dismiss();
                }

                // Generates an Alert Dialog to show the error message
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        ImageTargets.this);
                builder
                        .setMessage(errorMessage)
                        .setTitle(getString(R.string.INIT_ERROR))
                        .setCancelable(false)
                        .setIcon(0)
                        .setPositiveButton(getString(R.string.button_OK),
                                new DialogInterface.OnClickListener()
                                {
                                    public void onClick(DialogInterface dialog, int id)
                                    {
                                        finish();
                                    }
                                });

                mErrorDialog = builder.create();
                mErrorDialog.show();
            }
        });
    }


    @Override
    public void onVuforiaUpdate(State state)
    {
        CameraDevice.getInstance().setFocusMode(CameraDevice.FOCUS_MODE.FOCUS_MODE_CONTINUOUSAUTO);
//        if (mSwitchDatasetAsap)
//        {
//            mSwitchDatasetAsap = false;
//            TrackerManager tm = TrackerManager.getInstance();
//            ObjectTracker ot = (ObjectTracker) tm.getTracker(ObjectTracker
//                    .getClassType());
//            if (ot == null || mCurrentDataset == null
//                    || ot.getActiveDataSet(0) == null)
//            {
//                Log.d(LOGTAG, "Failed to swap datasets");
//                return;
//            }
//
//            doUnloadTrackersData();
//            doLoadTrackersData();
//        }
    }


    @Override
    public boolean doInitTrackers()
    {
        // Indicate if the trackers were initialized correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        Tracker tracker;

        // Trying to initialize the image tracker
        tracker = tManager.initTracker(ObjectTracker.getClassType());
        if (tracker == null)
        {
            Log.e(
                    LOGTAG,
                    "Tracker not initialized. Tracker already initialized or the camera is already started");
            result = false;
        } else
        {
            Log.i(LOGTAG, "Tracker successfully initialized");
        }
        return result;
    }


    @Override
    public boolean doStartTrackers()
    {
        // Indicate if the trackers were started correctly
        boolean result = true;

        Tracker objectTracker = TrackerManager.getInstance().getTracker(
                ObjectTracker.getClassType());
        if (objectTracker != null)
            objectTracker.start();

        return result;
    }


    @Override
    public boolean doStopTrackers()
    {
        // Indicate if the trackers were stopped correctly
        boolean result = true;

        Tracker objectTracker = TrackerManager.getInstance().getTracker(
                ObjectTracker.getClassType());
        if (objectTracker != null)
            objectTracker.stop();

        return result;
    }


    @Override
    public boolean doDeinitTrackers()
    {
        // Indicate if the trackers were deinitialized correctly
        boolean result = true;

        TrackerManager tManager = TrackerManager.getInstance();
        tManager.deinitTracker(ObjectTracker.getClassType());

        return result;
    }


    @Override
    public boolean onTouchEvent(MotionEvent event)
    {

        return mGestureDetector.onTouchEvent(event);
    }


    boolean isExtendedTrackingActive()
    {
        return mExtendedTracking;
    }

}
