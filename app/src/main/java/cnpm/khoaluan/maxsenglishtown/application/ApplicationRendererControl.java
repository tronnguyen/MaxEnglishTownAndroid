package cnpm.khoaluan.maxsenglishtown.application;

import com.vuforia.State;

/**
 * Created by Dell on 3/16/2018.
 */

public interface ApplicationRendererControl {

    // This method has to be implemented by the Renderer class which handles the content rendering
    // of the sample, this one is called from SampleAppRendering class for each view inside a loop
    void renderFrame(State state, float[] projectionMatrix);

}
