package cnpm.khoaluan.maxsenglishtown.activities

import android.media.MediaPlayer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.*
import cnpm.khoaluan.maxsenglishtown.R
import cnpm.khoaluan.maxsenglishtown.global.Global
import cnpm.khoaluan.maxsenglishtown.global.Constants
import java.util.*

class MemoryMatchActivity : AppCompatActivity() {

    private var cardBtn1 : ImageButton? = null
    private var cardBtn2 : ImageButton? = null
    private var cardBtn3 : ImageButton? = null
    private var cardBtn4 : ImageButton? = null
    private var cardBtn5 : ImageButton? = null
    private var cardBtn6 : ImageButton? = null
    private var iCount = 0
    private var pack : ArrayList<String>? = null
    private var strCard1  = ""
    private var strCard2  = ""
    private var strCard3  = ""
    private var strCard4  = ""
    private var strCard5  = ""
    private var strCard6  = ""
    private var numberClick = 0
    private var soundRight : MediaPlayer? = null
    private var soundWrong : MediaPlayer? = null
    private var soundClick : MediaPlayer? = null
    private var countTime = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_memory_match)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
//        var layout : RelativeLayout = findViewById(R.id.layout_title_memory)
//        var sdk = android.os.Build.VERSION.SDK_INT
//        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
//            layout.setBackgroundDrawable(ContextCompat.getDrawable(baseContext, R.drawable.titlegamescreen) )
//        } else {
//            layout.setBackground(ContextCompat.getDrawable(baseContext, R.drawable.titlegamescreen))
//        }
        //Sound
        soundClick = MediaPlayer()
        var afd3 = assets.openFd("sounds/effects/ClickButton.mp3")
        soundClick?.setDataSource(afd3.fileDescriptor,afd3.startOffset, afd3.length)
        afd3.close()
        soundClick?.prepare()

        soundRight = MediaPlayer()
        var afd = assets.openFd("sounds/effects/MemoryMatchCorrectFlip.mp3")
        soundRight?.setDataSource(afd.fileDescriptor,afd.startOffset, afd.length)
        afd.close()
        soundRight?.prepare()

        soundWrong = MediaPlayer()
        var afd2 = assets.openFd("sounds/effects/MemoryMatchWrongFlip.mp3")
        soundWrong?.setDataSource(afd2.fileDescriptor,afd2.startOffset, afd2.length)
        afd2.close()
        soundWrong?.prepare()

        //Load Data
        pack = Global.ParseXML(Constants.PACK_ID,this,false)
        //Init UI
//        var txtTitle : TextView = findViewById(R.id.txt_title)
//        var imageFooder : ImageView = findViewById(R.id.image_fooder)
        var backBtn : ImageView = findViewById(R.id.btn_back_to_card)
        cardBtn1  = findViewById(R.id.btn_card1)
        cardBtn2  = findViewById(R.id.btn_card2)
        cardBtn3  = findViewById(R.id.btn_card3)
        cardBtn4  = findViewById(R.id.btn_card4)
        cardBtn5  = findViewById(R.id.btn_card5)
        cardBtn6  = findViewById(R.id.btn_card6)
        //Config UI
        //Set Listener
        backBtn.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                soundClick?.start()
                onBackPressed()
            }
        })
        cardBtn1!!.setOnClickListener {
            numberClick = 1
            onClickButton(cardBtn1!!,strCard1)
            isPassed()
        }
        cardBtn2!!.setOnClickListener {
            numberClick = 2
            onClickButton(cardBtn2!!,strCard2)
            isPassed()
        }
        cardBtn3!!.setOnClickListener {
            numberClick = 3
            onClickButton(cardBtn3!!,strCard3)
            isPassed()
        }
        cardBtn4!!.setOnClickListener {
            numberClick = 4
            onClickButton(cardBtn4!!,strCard4)
            isPassed()
        }
        cardBtn5!!.setOnClickListener {
            numberClick = 5
            onClickButton(cardBtn5!!,strCard5)
            isPassed()
        }
        cardBtn6!!.setOnClickListener {
            numberClick = 6
            onClickButton(cardBtn6!!,strCard6)
            isPassed()
        }
        theNextQuestion()
    }
    private fun onClickButton(btn : ImageButton, str : String){
        if(Constants.BEFORE_CARD != Constants.RES_FONTOFCARD && Constants.BEFORE_CARD == str){
            soundRight?.start()
            btn.setImageResource(GetIdFromString(str))
            btn.isEnabled = false
            Constants.BEFORE_CARD = Constants.RES_FONTOFCARD
            Constants.TEXT_DONE = str
            if(numberClick == 1)
                strCard1 = "done"
            else if(numberClick == 2)
                strCard2 = "done"
            else if(numberClick == 3)
                strCard3 = "done"
            else if(numberClick == 4)
                strCard4 = "done"
            else if(numberClick == 5)
                strCard5 = "done"
            else if(numberClick == 6)
                strCard6 = "done"

            Constants.IS_DONE ++
        }
        else if(Constants.BEFORE_CARD == Constants.RES_FONTOFCARD){
            soundClick?.start()
            btn.setImageResource(GetIdFromString(str))
            //btn!!.touchable = Touchable.disabled
            Constants.BEFORE_CARD = str
        }
        else if(Constants.BEFORE_CARD != Constants.RES_FONTOFCARD && Constants.BEFORE_CARD != str){
            soundWrong?.start()
            Constants.NOT_MATCH = true
            btn.setImageResource(GetIdFromString(str))
            btn.isEnabled = true
            Constants.BEFORE_CARD = Constants.RES_FONTOFCARD
        }
    }
    private fun GetIdFromString(s: String): Int {
        Log.e("Debug", s)
        return resources.getIdentifier(s, "drawable", packageName)
    }
    private fun isPassed(){
        if(Constants.NOT_MATCH){
            if(strCard1 != "done"){
                if(strCard1 == Constants.TEXT_DONE){
                    strCard1 = "done"
                }
                else {
                    cardBtn1?.postDelayed(Runnable {
                        cardBtn1?.setImageResource(GetIdFromString(Constants.RES_FONTOFCARD))
                    },500L)

                }
            }
            if(strCard2 != "done"){
                if(strCard2 == Constants.TEXT_DONE){
                    strCard2 = "done"
                }
                else {
                    cardBtn2?.postDelayed(Runnable {
                        cardBtn2?.setImageResource(GetIdFromString(Constants.RES_FONTOFCARD))
                    },500L)

                }
            }
            if(strCard3 != "done"){
                if(strCard3 == Constants.TEXT_DONE){
                    strCard3 = "done"
                }
                else {
                    cardBtn3?.postDelayed(Runnable {
                        cardBtn3?.setImageResource(GetIdFromString(Constants.RES_FONTOFCARD))
                    },500L)

                }
            }
            if(strCard4 != "done"){
                if(strCard4 == Constants.TEXT_DONE){
                    strCard4 = "done"
                }
                else {
                    cardBtn4?.postDelayed(Runnable {
                        cardBtn4?.setImageResource(GetIdFromString(Constants.RES_FONTOFCARD))
                    },500L)

                }
            }
            if(strCard5 != "done"){
                if(strCard5 == Constants.TEXT_DONE){
                    strCard5 = "done"
                }
                else {
                    cardBtn5?.postDelayed(Runnable {
                        cardBtn5?.setImageResource(GetIdFromString(Constants.RES_FONTOFCARD))
                    },500L)

                }
            }
            if(strCard6 != "done"){
                if(strCard6 == Constants.TEXT_DONE){
                    strCard6 = "done"
                }
                else {
                    cardBtn6?.postDelayed(Runnable {
                        cardBtn6?.setImageResource(GetIdFromString(Constants.RES_FONTOFCARD))
                    },500L)

                }
            }
            Constants.NOT_MATCH = false
        }
        if(Constants.IS_DONE == 3){
            countTime +=3
            if(countTime == 9 && !Constants.getValue(9,"trophy10")){
//                Constants.saveDataRewards(9,baseContext)
//                var dialog : RewardDialog = RewardDialog(this@MemoryMatchActivity,true, 10)
//                dialog.show()
                Constants.rewards[9] = true
            }
            iCount += 3
            val handler = Handler(Looper.getMainLooper())
            handler.postDelayed(Runnable {
                theNextQuestion()
            },(1500).toLong())

        }
    }
    fun theNextQuestion(){
        Constants.IS_DONE = 0
        var n1 = Random().nextInt(3)
        var n2 = Random().nextInt(3)
        while(n2 == n1){
            n2 = Random().nextInt(3)
        }
        var n3 = Random().nextInt(3)
        while(n3 == n2 || n3 == n1){
            n3 = Random().nextInt(3)
        }
        if(iCount==24){
            iCount=23
        }
        strCard1  = pack!![iCount + n1]
        strCard2  = pack!![iCount + n2]
        strCard3  = pack!![iCount + n3]
        n1 = Random().nextInt(3)
        n2 = Random().nextInt(3)
        while(n2 == n1){
            n2 = Random().nextInt(3)
        }
        n3 = Random().nextInt(3)
        while(n3 == n2 || n3 == n1){
            n3 = Random().nextInt(3)
        }
        strCard4  = pack!![iCount + n2]
        strCard5  = pack!![iCount + n1]
        strCard6  = pack!![iCount + n3]

        cardBtn1?.setImageResource(GetIdFromString(Constants.RES_FONTOFCARD))
        cardBtn2?.setImageResource(GetIdFromString(Constants.RES_FONTOFCARD))
        cardBtn3?.setImageResource(GetIdFromString(Constants.RES_FONTOFCARD))
        cardBtn4?.setImageResource(GetIdFromString(Constants.RES_FONTOFCARD))
        cardBtn5?.setImageResource(GetIdFromString(Constants.RES_FONTOFCARD))
        cardBtn6?.setImageResource(GetIdFromString(Constants.RES_FONTOFCARD))

        cardBtn1?.isEnabled = true
        cardBtn2?.isEnabled = true
        cardBtn3?.isEnabled = true
        cardBtn4?.isEnabled = true
        cardBtn5?.isEnabled = true
        cardBtn6?.isEnabled = true

    }
    override fun onStop() {
        super.onStop()
        soundRight?.stop()
        soundWrong?.stop()
        soundClick?.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        soundRight?.release()
        soundWrong?.release()
        soundClick?.stop()
    }
}
