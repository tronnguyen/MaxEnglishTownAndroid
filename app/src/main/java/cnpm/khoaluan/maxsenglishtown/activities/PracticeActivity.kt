package cnpm.khoaluan.maxsenglishtown.activities

import android.animation.ObjectAnimator
import android.content.Intent
import android.graphics.drawable.Drawable
import android.media.MediaPlayer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.View
import android.view.WindowManager
import android.view.animation.AnimationUtils
import android.widget.ImageView
import cnpm.khoaluan.maxsenglishtown.R
import cnpm.khoaluan.maxsenglishtown.global.Constants
import cnpm.khoaluan.maxsenglishtown.global.Global
import kotlinx.android.synthetic.main.activity_practice.*
import java.io.InputStream

class PracticeActivity : AppCompatActivity() {

    private var touchCount = 0
    private var imageGift : ImageView? = null
    private var soundShowCard : MediaPlayer? = null
    private var soundClick : MediaPlayer? = null
    private var soundBouncingPresent : MediaPlayer? = null
    private var soundExplodingPresent : MediaPlayer? = null
    private var soundGreatJob : MediaPlayer? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practice)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        //Sound
        soundGreatJob = MediaPlayer()
        var afd5 = assets.openFd("sounds/alphabets/greatjob.mp3")
        soundGreatJob?.setDataSource(afd5.fileDescriptor,afd5.startOffset, afd5.length)
        afd5.close()
        soundGreatJob?.prepare()

        soundClick = MediaPlayer()
        var afd3 = assets.openFd("sounds/effects/ClickButton.mp3")
        soundClick?.setDataSource(afd3.fileDescriptor,afd3.startOffset, afd3.length)
        afd3.close()
        soundClick?.prepare()

        soundBouncingPresent = MediaPlayer()
        var afd = assets.openFd("sounds/effects/BouncingPresent.mp3")
        soundBouncingPresent?.setDataSource(afd.fileDescriptor,afd.startOffset, afd.length)
        afd.close()
        soundBouncingPresent?.prepare()

        soundExplodingPresent = MediaPlayer()
        var afd2 = assets.openFd("sounds/effects/ExplodingPresent.mp3")
        soundExplodingPresent?.setDataSource(afd2.fileDescriptor,afd2.startOffset, afd2.length)
        afd2.close()
        soundExplodingPresent?.prepare()

        soundShowCard = MediaPlayer()
        var afd4 = assets.openFd("sounds/effects/ShowCard.mp3")
        soundShowCard?.setDataSource(afd4.fileDescriptor,afd4.startOffset, afd4.length)
        afd4.close()
        soundShowCard?.prepare()
        //Init UI
        var backBtn : ImageView = findViewById(R.id.btn_back_to_learn)
        imageGift = findViewById(R.id.gift)
        val leftShake = AnimationUtils.loadAnimation(this, R.anim.anim_left_shake)
        leftShake.repeatCount = ObjectAnimator.INFINITE
        leftShake.repeatMode = ObjectAnimator.REVERSE
        imageGift?.animation = leftShake
        //Config UI
        imageGift!!.setImageResource(R.drawable.normal)
        //Set Listener
        backBtn.visibility = View.GONE
        backBtn.setOnClickListener {
            soundClick?.start()
            var pack = Global.ParseXML(Constants.PACK_ID,this@PracticeActivity,false)
                when(Constants.PACK_ID) {
                    "data_alphabet.xml"-> {
                        if(Constants.CARD != "Alphabet-Z") {
                            for (i in 0..pack!!.size) {
                                if (pack[i].endsWith(Constants.CARD)) {
                                    var strTemp = pack[i + 1]
                                    Constants.jsonObjectData!!.getJSONArray("Alphabet").getJSONObject(i+1).remove(strTemp)
                                    Constants.jsonObjectData!!.getJSONArray("Alphabet").getJSONObject(i+1).put(strTemp,true)
                                    var str = Constants.jsonObjectData.toString()
                                    Global.saveData(baseContext,str)
                                    break
                                }
                            }
                        }
                    }
                    "data_animals.xml"->{
                        if(Constants.CARD != "Card-Tutle") {
                            for (i in 0..pack!!.size) {
                                if (pack[i].endsWith(Constants.CARD)) {
                                    var strTemp = pack[i + 1]
                                    Constants.jsonObjectData!!.getJSONArray("Animals").getJSONObject(i+1).remove(strTemp)
                                    Constants.jsonObjectData!!.getJSONArray("Animals").getJSONObject(i+1).put(strTemp,true)
                                    var str = Constants.jsonObjectData.toString()
                                    Global.saveData(baseContext,str)
                                    break
                                }
                            }
                        }
                    }
                }
            for(i in 0..Constants.packAlphabet!!.size){
                if(Constants.packAlphabet!!.get(i).endsWith(Constants.CARD)){
                    Constants.CARD = Constants.packAlphabet!!.get(i + 1)
                    break
                }
            }
            startActivity(Intent(baseContext,CardActivity::class.java))
        }
        imageGift!!.setOnClickListener {
            touchCount++
            if(touchCount < 3) {
                soundBouncingPresent?.start()
                imageGift!!.setImageResource(R.drawable.jump)
                imageGift!!.postDelayed(Runnable {
                    imageGift!!.setImageResource(R.drawable.normal)
                }, 1000)
            }
            if(touchCount == 3) {
                soundExplodingPresent?.start()
                imageGift!!.setImageResource(R.drawable.open)
                imageGift!!.postDelayed(Runnable {
                    soundShowCard?.start()
                    imageGift!!.setImageResource(resources.getIdentifier(Constants.CARD, "drawable", packageName))
                }, 2500)
            }
            if(touchCount == 4) {
                imageGift!!.postDelayed(Runnable {
                    soundGreatJob?.start()
                    //imageGift!!.setImageResource(R.drawable.goodjob)
                    imageGift?.animation = null
                    imageGift!!.visibility = View.INVISIBLE
                    layout_practice.background =(ContextCompat.getDrawable(baseContext, R.drawable.goodjob) )
                    backBtn.visibility = View.VISIBLE
                }, 1000)

            }
        }
    }
    private fun getDrawable(name :String, type : Int) : Drawable {
        var ims : InputStream? = null
        if(1 == type){
            ims = assets.open(Constants.PATH_IMAGES_OTHERS + name)
        }
        else if(2 == type){
            ims = assets.open(Constants.PATH_IMAGES_BUTTON + name)
        }
        else if(3 == type){
            ims = assets.open(name)
        }
        // load image as Drawable
        val dra = Drawable.createFromStream(ims, null)
        return dra
    }
    override fun onStop() {
        super.onStop()
        soundBouncingPresent?.stop()
        soundExplodingPresent?.stop()
        soundClick?.stop()
        soundShowCard?.stop()
        soundGreatJob?.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        soundBouncingPresent?.release()
        soundExplodingPresent?.release()
        soundClick?.release()
        soundShowCard?.release()
        soundGreatJob?.release()

    }

    override fun onBackPressed() {
        startActivity(Intent(this,LessonActivity::class.java))
    }
}
