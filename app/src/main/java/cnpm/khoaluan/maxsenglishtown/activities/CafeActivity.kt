package cnpm.khoaluan.maxsenglishtown.activities

import android.animation.ObjectAnimator
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import cnpm.khoaluan.maxsenglishtown.R
import cnpm.khoaluan.maxsenglishtown.application.ImageTargets
import cnpm.khoaluan.maxsenglishtown.global.Global
import cnpm.khoaluan.maxsenglishtown.global.Constants
import cnpm.khoaluan.maxsenglishtown.springy.facebook.rebound.SpringAnimationType
import cnpm.khoaluan.maxsenglishtown.springy.facebook.rebound.SpringyAnimator
import com.bumptech.glide.Glide
import com.facebook.rebound.SpringSystem
import com.vuforia.*
import org.json.JSONObject
import java.util.*
import kotlinx.android.synthetic.main.activity_cafe.*
import kotlin.collections.ArrayList


class CafeActivity : ImageTargets() {
    private var height : Float = 0f
    private var width : Float = 0f
    private var tempCharacter = ""
    private var noodles : ArrayList<String>? = null
    private var soups : ArrayList<String>? = null
    private var otherFoods : ArrayList<String>? = null
    private var drinks : ArrayList<String>? = null
    private var arrayAdded : ArrayList<String>? = null
    private var strNoodle = ""
    private var strSoup = ""
    private var strOtherFood  = ""
    private var strDrink  = ""
    private var strOrderFoodFinish = ""
    private var strOrderDrinkFinish = ""
    private var handlerThread : Handler? = null
    private var runnablePasta : Runnable? = null
    private var runnableTopping : Runnable? = null
    private var runnableSauce : Runnable? = null
    private var runnableDrink : Runnable? = null
    private var runnableImageView : Runnable? = null
    private var isStart = false
    private var isDrink = false
    private var isPasta = false
    private var isSauce = false
    private var isTopping = false
    private var anim : Animation?= null
    private var strTempPasta = ""
    private var strTempSauce = ""
    private var countTrue = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        SetUILayout(R.layout.activity_cafe)
        super.onCreate(savedInstanceState)
        Constants.SpringSystemManager = SpringSystem.create()
        handlerThread = Handler(android.os.Looper.getMainLooper())
        height = resources
                .displayMetrics
                .heightPixels.toFloat()
        width = resources
                .displayMetrics
                .widthPixels.toFloat()
        //Add data sets
        getmDatasetStrings().add("data_pastastation_table.xml")
        getmDatasetStrings().add("data_pastastation_pasta.xml")
        getmDatasetStrings().add("data_pastastation_sauce.xml")
        getmDatasetStrings().add("data_pastastation_topping.xml")
        getmDatasetStrings().add("data_pastastation_drink.xml")
        tempCharacter = ""
        initData()
        anim = AnimationUtils.loadAnimation(this, R.anim.anim_scale)
        anim?.repeatCount = ObjectAnimator.INFINITE
        anim?.repeatMode = ObjectAnimator.REVERSE
        btn_back.setOnClickListener{
            if(isLoaded){
                onBackPressed()
            }
        }
    }

    override fun getClassName(): String {
        return "CafeActivity"
    }
    private fun initData(){
        arrayAdded = ArrayList()
        noodles = ArrayList()
        otherFoods = ArrayList()
        soups = ArrayList()
        drinks = ArrayList()
        img_pasta.animation = null
        img_sauce.animation = null
        //Read data
        var dataString = isOpen()
        var objectCafe = JSONObject(dataString)
        var jsonArrayData = objectCafe.getJSONArray("noodle")
        for(index in 0..(jsonArrayData.length()-1)){
            noodles!!.add(jsonArrayData.getString(index))
        }
        jsonArrayData = objectCafe.getJSONArray("soup")
        for(index in 0..(jsonArrayData.length()-1)){
            soups!!.add(jsonArrayData.getString(index))
        }
        jsonArrayData = objectCafe.getJSONArray("other_food")
        for(index in 0..(jsonArrayData.length()-1)){
            otherFoods!!.add(jsonArrayData.getString(index))
        }
        jsonArrayData = objectCafe.getJSONArray("drink")
        for(index in 0..(jsonArrayData.length()-1)){
            drinks!!.add(jsonArrayData.getString(index))
        }
    }
    private fun isOpen() : String{
        var json = Global.getDataCafe(baseContext)
        return json!!
    }
    private fun theNextOrder(){
        countTrue=0
        doUnloadTrackersData()
        doLoadTrackersData()
        arrayAdded?.clear()
        layout_order.postDelayed(Runnable {
            layout_order.visibility = View.INVISIBLE
        },0L)

        moveLeft(img_pet, arrayListOf("dog_cafe","panda_cafe","cat_cafe")[(Random().nextInt(3))])
        img_pasta.postDelayed(Runnable {
            img_pasta.visibility = View.INVISIBLE
            img_pasta_check.visibility = View.INVISIBLE
            img_sauce.visibility = View.INVISIBLE
            img_sauce_check.visibility = View.INVISIBLE
            img_topping.visibility = View.INVISIBLE
            img_topping_check.visibility = View.INVISIBLE
            img_drink.visibility = View.INVISIBLE
            img_drink_check.visibility = View.INVISIBLE
            img_drink_finish.visibility = View.INVISIBLE
            img_food.visibility = View.INVISIBLE
            img_coins.visibility = View.INVISIBLE
        },0L)

        isDrink = false
        isPasta = false
        isSauce = false
        isTopping = false
        val noodle = Random().nextInt(noodles!!.size)
        val soup = Random().nextInt(soups!!.size)
        val food = Random().nextInt(otherFoods!!.size)
        val drink = Random().nextInt(drinks!!.size)
        strNoodle = noodles!![noodle]
        strSoup = soups!![soup]
        strOtherFood = otherFoods!![food]
        strDrink = drinks!![drink]
        orderAnimation()
        img_food_start.postDelayed({
            moveUp(img_food_start,"food000",false)
            moveUp(img_drink_start,"drink0",false)
        },3500L)
    }
    private fun orderAnimation(){
        runnablePasta = Runnable {
            img_pasta.visibility = View.VISIBLE
            img_pasta_check.visibility = View.VISIBLE
            img_pasta.setImageResource(getIdFromString(strNoodle)!!)
            img_pasta_check.setImageResource(getIdFromString("num1")!!)
        }
        handlerThread?.postDelayed(runnablePasta,1500L)
        runnableSauce = Runnable {

            img_sauce.visibility = View.VISIBLE
            img_sauce_check.visibility = View.VISIBLE
            img_sauce.setImageResource(getIdFromString(strSoup)!!)
            img_sauce_check.setImageResource(getIdFromString("num2")!!)
        }
        handlerThread?.postDelayed(runnableSauce,2000L)
        runnableTopping = Runnable {
            img_topping.visibility = View.VISIBLE
            img_topping_check.visibility = View.VISIBLE
            img_topping.setImageResource(getIdFromString(strOtherFood)!!)
            img_topping_check.setImageResource(getIdFromString("num3")!!)
        }
        handlerThread?.postDelayed(runnableTopping,2500L)
        runnableDrink = Runnable {
            img_drink.visibility = View.VISIBLE
            img_drink_check.visibility = View.VISIBLE
            img_drink.setImageResource(getIdFromString(strDrink)!!)
            img_drink_check.setImageResource(getIdFromString("num4")!!)
        }
        handlerThread?.postDelayed(runnableDrink,3000L)
    }
    override fun onResume() {
        super.onResume()
        tempCharacter = ""
        Constants.SpringSystemManager = SpringSystem.create()
    }
    private fun chageDataSet(index : Int){
        setmCurrentDatasetSelectionIndex(index)
        doUnloadTrackersData()
        doLoadTrackersData()
    }
    override fun onVuforiaUpdate(state: State?) {
        super.onVuforiaUpdate(state)
        if(state != null) {
            var number = state.numTrackableResults
            for (tIdx in 0 until number) {
                val result = state.getTrackableResult(tIdx)
                val trackable = result.trackable
                var tempCard = trackable.name.toString()
                if (!tempCharacter.endsWith(tempCard)){
                    if(tempCard.endsWith("table") && !isStart){
                        isStart = true
                        theNextOrder()
                        chageDataSet(1)
                        tempCharacter = tempCard
                        break
                    }
                    if(isStart && drinks!!.contains(tempCard) && !isDrink){
                        isDrink = true
                        img_drink_check.postDelayed(Runnable {
                            img_drink_check.visibility = View.VISIBLE
                        },0L)
                        tempCharacter = tempCard
                        if(tempCard.endsWith(strDrink)){
                            countTrue++
                            chageDataSet(0)
                            img_drink_check.postDelayed(Runnable {
                                img_drink_check.setImageResource(getIdFromString("correct_ingredient")!!)
                            },0L)

                            moveUp(img_emotion,"right",true)
                            strOrderDrinkFinish = "drink" + (drinks!!.indexOf(strDrink) + 1).toString()
                            moveUp(img_drink_start,strOrderDrinkFinish,false)
                        }
                        else{
                            chageDataSet(0)
                            img_drink_check.postDelayed(Runnable {
                                img_drink_check.setImageResource(getIdFromString("wrong_ingredient")!!)
                            },0L)
                            moveUp(img_emotion,"wrong",true)
                            strOrderDrinkFinish = "drink" + (drinks!!.indexOf(tempCard) + 1).toString()
                            moveUp(img_drink_start,strOrderDrinkFinish,false)
                        }
                        if(isTopping){
                            img_food.postDelayed(Runnable {
                                img_food_start.visibility = View.INVISIBLE
                                img_drink_start.visibility = View.INVISIBLE
                                moveUp(img_food,strOrderFoodFinish,false)
                                moveUp(img_drink_finish,strOrderDrinkFinish,false)
                            },2000L)
                            img_coins.postDelayed(Runnable {
                                if(countTrue==0 || countTrue==1) {
                                    moveUp(img_emotion, "result_01", true)
                                }
                                else if(countTrue==2 || countTrue==3) {
                                    moveUp(img_coins,"coins",false)
                                    moveUp(img_emotion, "result_23", true)
                                }
                                else if(countTrue==4) {
                                    moveUp(img_coins,"coins",false)
                                    moveUp(img_emotion, "result_4", true)
                                }
                            },3000L)
                            img_coins.postDelayed(Runnable {
                                isStart = false
                            },3500L)
                        }
                    }
                    else if(isStart){
                        if (noodles!!.contains(tempCard) && !isPasta) {
                            tempCharacter = tempCard
                            isPasta = true
                            img_pasta.animation = null
                            if (tempCard.endsWith(strNoodle)) {
                                countTrue++
                                chageDataSet(2)
                                strTempPasta = strNoodle
                                moveUp(img_emotion,"right",true)
                                img_pasta_check.postDelayed(Runnable {
                                    img_pasta_check.setImageResource(getIdFromString("correct_ingredient")!!)
                                },0L)
                                strOrderFoodFinish = "food" + (noodles!!.indexOf(strNoodle) + 1).toString() +"00"
                                moveUp(img_food_start,strOrderFoodFinish,false)
                            } else {
                                chageDataSet(2)
                                moveUp(img_emotion,"wrong",true)
                                img_pasta_check.postDelayed(Runnable {
                                    img_pasta_check.setImageResource(getIdFromString("wrong_ingredient")!!)
                                },0L)

                                strTempPasta = tempCard
                                strOrderFoodFinish = "food" + (noodles!!.indexOf(tempCard) + 1).toString() +"00"
                                moveUp(img_food_start,strOrderFoodFinish,false)
                            }
                        }
                        if (soups!!.contains(tempCard)) {
                            if(!isSauce && isPasta) {
                                img_sauce.animation = null
                                isSauce = true
                                tempCharacter = tempCard
                                if (tempCard.endsWith(strSoup)) {
                                    countTrue++
                                    chageDataSet(3)
                                    strTempSauce = strSoup
                                    moveUp(img_emotion,"right",true)
                                    img_sauce_check.postDelayed(Runnable {
                                        img_sauce_check.setImageResource(getIdFromString("correct_ingredient")!!)
                                    },0L)

                                    strOrderFoodFinish = "food" + (noodles!!.indexOf(strTempPasta) + 1).toString() + (soups!!.indexOf(strSoup) + 1).toString() +"0"
                                    moveUp(img_food_start,strOrderFoodFinish,false)
                                } else {
                                    chageDataSet(3)
                                    strTempSauce = tempCard
                                    moveUp(img_emotion,"wrong",true)
                                    strOrderFoodFinish = "food" + (noodles!!.indexOf(strTempPasta) + 1).toString() + (soups!!.indexOf(tempCard) + 1).toString() +"0"
                                    img_sauce_check.postDelayed(Runnable {
                                        img_sauce_check.setImageResource(getIdFromString("wrong_ingredient")!!)
                                    },0L)
                                    moveUp(img_food_start,strOrderFoodFinish,false)
                                }
                            }
                            else{
                                if(img_pasta.animation == null) {
                                    img_pasta.startAnimation(anim)
                                }
                            }
                        }
                        if (otherFoods!!.contains(tempCard)) {
                            if(!isTopping && isSauce && isPasta) {
                                isTopping = true
                                if(isDrink){
                                    isStart = false
                                }
                                tempCharacter = tempCard
                                if (tempCard.endsWith(strOtherFood)) {
                                    countTrue++
                                    chageDataSet(4)
                                    moveUp(img_emotion,"right",true)
                                    img_topping_check.postDelayed(Runnable {
                                        img_topping_check.setImageResource(getIdFromString("correct_ingredient")!!)
                                    },0L)

                                    strOrderFoodFinish = "food" + (noodles!!.indexOf(strTempPasta) + 1).toString() + (soups!!.indexOf(strTempSauce) + 1).toString() + (otherFoods!!.indexOf(strOtherFood) + 1).toString()
                                    moveUp(img_food_start,strOrderFoodFinish,false)
                                } else {
                                    chageDataSet(4)
                                    moveUp(img_emotion,"wrong",true)
                                    img_topping_check.postDelayed(Runnable {
                                        img_topping_check.setImageResource(getIdFromString("wrong_ingredient")!!)
                                    },0L)
                                    strOrderFoodFinish = "food" + (noodles!!.indexOf(strTempPasta) + 1).toString() + (soups!!.indexOf(strTempSauce) + 1).toString() + (otherFoods!!.indexOf(tempCard) + 1).toString()
                                    moveUp(img_food_start,strOrderFoodFinish,false)
                                }
                            }
                            else if(!isPasta && img_pasta.animation == null){
                                img_pasta.startAnimation(anim)
                            }
                            else if(!isSauce && img_sauce.animation == null){
                                img_sauce.startAnimation(anim)
                            }
                        }
                    }
                }
            }
        }
    }
    private fun moveUp(imageView : ImageView, strRes: String, hide : Boolean){
                    val spring3 = SpringyAnimator(SpringAnimationType.TRANSLATEY,
                            0f, 0f, height/2, 0f, Constants.SpringSystemManager)
        handlerThread?.removeCallbacks(runnableImageView)
        imageView.postDelayed(Runnable {
            imageView.visibility = View.INVISIBLE
        },0L)
        imageView.postDelayed(Runnable {
            spring3.startSpring(imageView)
            imageView.visibility = View.VISIBLE
        }, (500).toLong())
        imageView.postDelayed(Runnable {
            Glide.with(baseContext)
                    .load(getIdFromString(strRes))
                    .into(imageView)
        },0L)
        if(hide) {
            runnableImageView = Runnable {
                imageView.visibility = View.INVISIBLE
            }
           handlerThread?.postDelayed(runnableImageView, (3000).toLong())
        }
    }
    private fun moveLeft(imageView : ImageView, strRes: String){
        val spring3 = SpringyAnimator(SpringAnimationType.TRANSLATEX,
                0f, 0f, width, 0f, Constants.SpringSystemManager)
        imageView.postDelayed(Runnable {
            imageView.visibility = View.INVISIBLE
        },0L)

        imageView.postDelayed(Runnable {
            spring3.startSpring(imageView)
            imageView.visibility = View.VISIBLE
        }, (500).toLong())
        imageView.postDelayed(Runnable {
            Glide.with(baseContext)
                    .load(getIdFromString(strRes))
                    .into(imageView)
        },0L)
        layout_order.postDelayed(Runnable {
            layout_order.visibility = View.VISIBLE
        },1000L)

    }
    //getIdFromString
    private fun getIdFromString(s: String): Int? {
        Log.e("Debug", s)
        return resources.getIdentifier(s, "drawable", packageName)
    }

    override fun onBackPressed() {
        if(isLoaded)
            super.onBackPressed()
    }
    override fun onDestroy() {
        super.onDestroy()
        handlerThread?.removeCallbacks(runnablePasta)
        handlerThread?.removeCallbacks(runnableSauce)
        handlerThread?.removeCallbacks(runnableTopping)
        handlerThread?.removeCallbacks(runnableDrink)
    }
}
