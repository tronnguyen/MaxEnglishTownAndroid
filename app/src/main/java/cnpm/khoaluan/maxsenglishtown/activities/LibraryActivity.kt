package cnpm.khoaluan.maxsenglishtown.activities

import android.media.MediaPlayer
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager
import android.widget.ImageButton;
import android.widget.ImageView
import android.widget.MediaController;
import android.widget.TextView;
import cnpm.khoaluan.maxsenglishtown.R
import cnpm.khoaluan.maxsenglishtown.global.Constants


class LibraryActivity : AppCompatActivity() {

    private val gifts = intArrayOf(R.drawable.busgif, R.drawable.kidgif, R.drawable.friendgif, R.drawable.elephantgif, R.drawable.liongif, R.drawable.beargif, R.drawable.giraffegif, R.drawable.rhinogif, R.drawable.monkeygif, R.drawable.busgif)
    private val stories = intArrayOf(R.string.page0, R.string.page1, R.string.page2, R.string.page3, R.string.page4, R.string.page5, R.string.page6, R.string.page7, R.string.page8, R.string.page9)
    private var pageNumber = 0
    private var soundClick : MediaPlayer? = null
    private var soundStory : MediaPlayer? = null
    private var isPlaying = false
    var backBtn : ImageButton? = null
    var nextBtn : ImageButton? = null
    var previousBtn : ImageButton? = null
    var playBtn : ImageButton? = null
    var resetBtn : ImageButton? = null
    var storiesTxt : TextView? = null
    var pageNumberTxt : TextView? = null
    var giftImageView : ImageView? = null
    var isFirstResume = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_library)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        //Sound
        soundClick = MediaPlayer()
        var afd3 = assets.openFd("sounds/effects/ClickButton.mp3")
        soundClick?.setDataSource(afd3.fileDescriptor,afd3.startOffset, afd3.length)
        afd3.close()
        soundClick?.prepare()
        soundStory = MediaPlayer()
         backBtn  = findViewById(R.id.btn_back)
         nextBtn  = findViewById(R.id.btn_next)
         previousBtn  = findViewById(R.id.btn_prev)
         playBtn  = findViewById(R.id.btn_play)
         resetBtn  = findViewById(R.id.btn_reset)
         storiesTxt = findViewById(R.id.tv_storyLine)
         pageNumberTxt  = findViewById(R.id.tv_pageNumber)
         giftImageView  = findViewById(R.id.gifImageView)
        //Config UI
        pageNumberTxt?.setText("Page " + (pageNumber + 1).toString() + "/10")
        storiesTxt?.setText(stories[0])
        giftImageView?.postDelayed(Runnable { giftImageView?.setImageResource(gifts[pageNumber]) }, 0L)
        previousBtn?.setEnabled(false)

        nextBtn?.setImageResource(R.drawable.icons8_next_96)
        previousBtn?.setImageResource(R.drawable.icons8_previous_96_dark)
        //Set Listener
        giftImageView?.setOnClickListener {
            if ((giftImageView?.getDrawable() as MediaController.MediaPlayerControl).isPlaying()) {
                (giftImageView?.getDrawable() as MediaController.MediaPlayerControl).pause()
            } else {
                (giftImageView?.getDrawable() as MediaController.MediaPlayerControl).start()
            }
        }

        playBtn?.setOnClickListener {
            soundStory?.release()
            soundStory = MediaPlayer()
            var afd = assets.openFd("sounds/librarys/ella_page"+ pageNumber +".mp3")
            soundStory?.setDataSource(afd.fileDescriptor,afd.startOffset, afd.length)
            afd.close()
            soundStory?.prepare()
            if(!isPlaying) {
                it.setBackgroundResource(R.drawable.icons8_pause_button_96)
                soundStory?.start()
                isPlaying = true
            }
            else{
                it.setBackgroundResource(R.drawable.icons8_circled_play_96)
                soundStory?.stop()
                isPlaying = false
            }
        }
        resetBtn?.visibility = View.VISIBLE
        resetBtn?.setOnClickListener {
            soundStory?.seekTo(0)
        }
        backBtn?.setOnClickListener { onBackPressed() }
        nextBtn?.visibility = View.INVISIBLE
        previousBtn?.visibility = View.INVISIBLE
        nextBtn?.setOnClickListener {
            playBtn?.setBackgroundResource(R.drawable.icons8_circled_play_96)
            if (pageNumber < 9)
                pageNumber++
            pageNumberTxt?.setText("Page " + (pageNumber + 1).toString() + "/10")
            storiesTxt?.setText(stories[pageNumber])
            giftImageView?.postDelayed(Runnable { giftImageView?.setImageResource(gifts[pageNumber]) }, 0L)
            (giftImageView?.getDrawable() as MediaController.MediaPlayerControl).pause()
            if (pageNumber == 9) {
                nextBtn?.setImageResource(R.drawable.icons8_next_96_dark)
                nextBtn?.setEnabled(false)
            } else if (pageNumber == 8) {
                nextBtn?.setEnabled(true)
                nextBtn?.setImageResource(R.drawable.icons8_next_96)
            }
            if (pageNumber == 1) {
                previousBtn?.setEnabled(true)
                previousBtn?.setImageResource(R.drawable.icons8_previous_96)
            }
            if(pageNumber == 9 && !Constants.getValue(8,"trophy9")){
//                    Constants.saveDataRewards(8,baseContext)
//                    var dialog : RewardDialog = RewardDialog(this@LibraryActivity,true, 9)
//                    dialog.show()
                Constants.rewards[8] = true
            }
        }
        previousBtn?.setOnClickListener {
            playBtn?.setBackgroundResource(R.drawable.icons8_circled_play_96)
            if (pageNumber > 0)
                pageNumber--
            pageNumberTxt?.setText("Page " + (pageNumber + 1).toString() + "/10")
            storiesTxt?.setText(stories[pageNumber])

            giftImageView?.postDelayed(Runnable { giftImageView?.setImageResource(gifts[pageNumber]) }, 0L)
            (giftImageView?.getDrawable() as MediaController.MediaPlayerControl).pause()
            if (pageNumber == 0) {
                previousBtn?.setImageResource(R.drawable.icons8_previous_96_dark)
                previousBtn?.setEnabled(false)
            } else if (pageNumber == 1) {
                previousBtn?.setEnabled(true)
                previousBtn?.setImageResource(R.drawable.icons8_previous_96)
            }
            if (pageNumber == 8) {
                nextBtn?.setEnabled(true)
                nextBtn?.setImageResource(R.drawable.icons8_next_96)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if(!isFirstResume) {
            isFirstResume = true
            nextBtn?.postDelayed({
                play()
            }, 500L)
            nextBtn?.postDelayed({
                next()
                play()
            }, 2000L)
            nextBtn?.postDelayed({
                next()
                play()
            }, 11000L)
            nextBtn?.postDelayed({
                next()
                play()
            }, 19000L)
            nextBtn?.postDelayed({
                next()
                play()
            }, 28000L)
            nextBtn?.postDelayed({
                next()
                play()
            }, 34000L)
            nextBtn?.postDelayed({
                next()
                play()
            }, 45000L)
            nextBtn?.postDelayed({
                next()
                play()
            }, 53000L)
            nextBtn?.postDelayed({
                next()
                play()
            }, 59000L)
            nextBtn?.postDelayed({
                next()
                play()
                nextBtn?.visibility = View.VISIBLE
                previousBtn?.visibility = View.VISIBLE
            }, 66000L)
        }
    }
    private fun next(){
        if (pageNumber < 9)
            pageNumber++
        pageNumberTxt?.setText("Page " + (pageNumber + 1).toString() + "/10")
        storiesTxt?.setText(stories[pageNumber])
        giftImageView?.postDelayed(Runnable { giftImageView?.setImageResource(gifts[pageNumber]) }, 0L)
        (giftImageView?.getDrawable() as MediaController.MediaPlayerControl).pause()
        if (pageNumber == 9) {
            nextBtn?.setImageResource(R.drawable.icons8_next_96_dark)
            nextBtn?.setEnabled(false)
        } else if (pageNumber == 8) {
            nextBtn?.setEnabled(true)
            nextBtn?.setImageResource(R.drawable.icons8_next_96)
        }
        if (pageNumber == 1) {
            previousBtn?.setEnabled(true)
            previousBtn?.setImageResource(R.drawable.icons8_previous_96)
        }
        if(pageNumber == 9 && !Constants.getValue(8,"trophy9")){
            Constants.rewards[8] = true
        }
    }
    private fun play(){
        soundStory?.release()
        soundStory = MediaPlayer()
        var afd = assets.openFd("sounds/librarys/ella_page"+ pageNumber +".mp3")
        soundStory?.setDataSource(afd.fileDescriptor,afd.startOffset, afd.length)
        afd.close()
        soundStory?.prepare()
        soundStory?.start()
        isPlaying = true
    }
    override fun onStop() {
        super.onStop()
        soundClick?.stop()
        soundStory?.stop()
    }
    override fun onDestroy() {
        super.onDestroy()
        soundClick?.release()
        soundStory?.release()
    }
}
