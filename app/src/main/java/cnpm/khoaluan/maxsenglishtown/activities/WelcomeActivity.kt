package cnpm.khoaluan.maxsenglishtown.activities

import android.content.Intent
import android.media.MediaPlayer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import cnpm.khoaluan.maxsenglishtown.global.Constants
import android.view.WindowManager
import android.widget.ImageButton
import cnpm.khoaluan.maxsenglishtown.R
import cnpm.khoaluan.maxsenglishtown.global.Global
import org.json.JSONObject

class WelcomeActivity : AppCompatActivity() {

    private var imagePlay : ImageButton? = null
    private var soundGameStart: MediaPlayer? = null
    private var soundWelcomeBackground : MediaPlayer? = null
    override fun onResume() {
        super.onResume()
        soundWelcomeBackground?.start()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        Constants.jsonObjectRewards = JSONObject(isOpen())
        Constants.jsonArrayRewards = Constants.jsonObjectRewards!!.getJSONArray("rewards")
        //Sound
        soundGameStart = MediaPlayer()
        var afd = assets.openFd("sounds/effects/GameStart.mp3")
        soundGameStart?.setDataSource(afd.fileDescriptor,afd.startOffset, afd.length)
        afd.close()
        soundGameStart?.prepare()

        soundWelcomeBackground = MediaPlayer()
        var afd3 = assets.openFd("sounds/effects/WelcomeBackgroundMusic.mp3")
        soundWelcomeBackground?.setDataSource(afd3.fileDescriptor,afd3.startOffset, afd3.length)
        afd3.close()
        soundWelcomeBackground?.prepare()
        soundWelcomeBackground?.start()
        Constants.loadData(this)
        imagePlay = findViewById(R.id.image_play_btn)
        //Set Listener
        imagePlay!!.setOnClickListener {
            soundGameStart?.start()
            soundWelcomeBackground?.stop()
            startActivity(Intent(baseContext,MapActivity::class.java))
        }
    }
    private fun isOpen() : String{
        var json = Global.getDataRewards(baseContext)
        return json!!
    }
    override fun onStop() {
        super.onStop()
        soundGameStart?.stop()
        soundWelcomeBackground?.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        soundGameStart?.release()
        soundWelcomeBackground?.release()
    }
}
