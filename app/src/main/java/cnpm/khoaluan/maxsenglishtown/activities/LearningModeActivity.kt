package cnpm.khoaluan.maxsenglishtown.activities

import android.animation.ObjectAnimator
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import cnpm.khoaluan.maxsenglishtown.R
import cnpm.khoaluan.maxsenglishtown.application.ImageTargets
import cnpm.khoaluan.maxsenglishtown.global.Global
import cnpm.khoaluan.maxsenglishtown.global.Constants
import com.vuforia.State
import org.json.JSONObject
import android.media.MediaPlayer
import android.view.animation.AnimationUtils


class LearningModeActivity : ImageTargets() {

    private var tempCharacter = ""
    private var layoutHeader : RelativeLayout? = null
    private var textViewCardName : TextView? = null
    private var btnSkip : ImageButton? = null
    private var mediaPlayer : MediaPlayer? = null
    private var moving  : MediaPlayer? = null
    private var imagePet : ImageView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        SetUILayout(R.layout.activity_learning_mode)
        super.onCreate(savedInstanceState)
        imagePet = findViewById(R.id.imageview_pet)
        //Set data
        getmDatasetStrings().add(Constants.PACK_ID)
        tempCharacter = Constants.CARD

        //Load data
        var words = Global.ParseXMLToHashTable("Alphabet.xml",this)!!
        var sampleRes: String
//        //Config UI
            btnSkip = findViewById(R.id.btn_skip)
//        layoutHeader = findViewById(R.id.layout_header)
//        layoutHeader!!.setBackgroundResource(R.drawable.dialog_9patch)
        textViewCardName = findViewById(R.id.textview_card_name)
        if(tempCharacter.length > 8 && tempCharacter.substring(0,8) == "alphabet"){
            textViewCardName!!.text = "Letter " + tempCharacter
                    .substring( tempCharacter.length - 1).toUpperCase()
            sampleRes =  words[tempCharacter.substring(tempCharacter.length - 1).toUpperCase()].toLowerCase()+"_"+tempCharacter.toLowerCase().substring(tempCharacter.length - 1)
        }else {
            textViewCardName!!.text = "Card " + tempCharacter.substring(5)
            sampleRes ="animal_"+tempCharacter.toLowerCase().substring(5).toLowerCase()
        }
        //Media
        mediaPlayer = MediaPlayer.create(this,R.raw.help)
        moving = MediaPlayer.create(baseContext, R.raw.movingon)
        //Read data
        var dataString = Global.getData(this)
        Constants.jsonObjectData = JSONObject(dataString)

        val shake = AnimationUtils.loadAnimation(this, R.anim.anim_shake)
        shake.repeatCount = ObjectAnimator.INFINITE
        shake.repeatMode = ObjectAnimator.REVERSE
        var imag : ImageView = findViewById(R.id.img_hint)
        imag.setImageResource(getIdResSample(sampleRes))
        imag.visibility = View.INVISIBLE
        imag.postDelayed({
            imag.visibility = View.VISIBLE
            imag.animation = shake
        },10000L)
        btnSkip!!.setOnClickListener {
            onNext()
            startActivity(Intent(baseContext,LessonActivity::class.java))
//                var pack = Global.ParseXML(Constants.PACK_ID,this@LearningModeActivity,false)
//                when(Constants.PACK_ID) {
//                    "data_alphabet.xml"-> {
//                        if(Constants.CARD != "Alphabet-Z") {
//                            for (i in 0..pack!!.size) {
//                                if (pack[i].endsWith(Constants.CARD)) {
//                                    var strTemp = pack[i + 1]
//                                    Constants.jsonObjectData!!.getJSONArray("Alphabet").getJSONObject(i+1).remove(strTemp)
//                                    Constants.jsonObjectData!!.getJSONArray("Alphabet").getJSONObject(i+1).put(strTemp,true)
//                                    var str = Constants.jsonObjectData.toString()
//                                    Global.saveData(baseContext,str)
//                                    break
//                                }
//                            }
//                        }
//                    }
//                    "data_animals.xml"->{
//                        if(Constants.CARD != "Card-Tutle") {
//                            for (i in 0..pack!!.size) {
//                                if (pack[i].endsWith(Constants.CARD)) {
//                                    var strTemp = pack[i + 1]
//                                    Constants.jsonObjectData!!.getJSONArray("Animals").getJSONObject(i+1).remove(strTemp)
//                                    Constants.jsonObjectData!!.getJSONArray("Animals").getJSONObject(i+1).put(strTemp,true)
//                                    var str = Constants.jsonObjectData.toString()
//                                    Global.saveData(baseContext,str)
//                                    break
//                                }
//                            }
//                        }
//                    }
//                }
//
//                tempCharacter = Constants.CARD
//                if(tempCharacter.length > 8 && tempCharacter.substring(0,8) == "Alphabet"){
//                    textViewCardName!!.text = "Letter " + tempCharacter
//                            .substring( tempCharacter.length - 1)
//                }else {
//                    textViewCardName!!.text = "Card " + tempCharacter.substring(5)
//                }
//                moving!!.start()
//                val handler = Handler(Looper.getMainLooper())
//                handler.postDelayed(Runnable {
//                    mediaPlayer!!.start()
//                    isPlay = true
//                },(1000).toLong())
        }

    }

    override fun getClassName(): String {
        return "LearningModeActivity"
    }
    override fun onBackPressed() {
        if(isLoaded) {
            super.onDestroy()
            startActivity(Intent(this, CardActivity::class.java))
        }
    }
    private fun getIdResSample(s: String): Int {
        Log.e("Debug", s)
        return resources.getIdentifier(s, "drawable", packageName)
    }
    override fun onDestroy() {
        mediaPlayer!!.release()
        moving!!.release()
        super.onDestroy()
    }
    private fun GetIdFromString(s: String): Int {
        Log.e("Debug", s)
        when(Constants.PACK_ID) {
            "data_alphabet.xml"-> {
                var word = "letter" + s.toLowerCase().substring(s.length - 1)
                return resources.getIdentifier(word, "raw", packageName)
            }
            "data_animals.xml"->{
                var word = "card" + s.toLowerCase().substring(5)
                return resources.getIdentifier(word, "raw", packageName)
            }
        }
        return 0
    }
    override fun onVuforiaUpdate(state: State?) {
        super.onVuforiaUpdate(state)
        //Detecting with camera
        if (isPlay && !mediaPlayer!!.isPlaying) {
            val letter = MediaPlayer.create(this, GetIdFromString(Constants.CARD))
            textViewCardName?.postDelayed(Runnable {
                letter.start()
            },500L)

            isPlay = false
        }
        if(state != null) {
            for (tIdx in 0 until state.getNumTrackableResults()) {
                val result = state.getTrackableResult(tIdx)
                val trackable = result.trackable
                var tempCard = trackable.name.toString()
                if (tempCharacter.endsWith(tempCard)) {
                    if(!Constants.getValue(0,"trophy1")){
//                        Constants.saveDataRewards(0,baseContext)
//                        var dialog : RewardDialog = RewardDialog(this,true, 1)
//                        dialog.show()
                        Constants.rewards[0] = true
                    }
                    Log.e("Debug",trackable.name.toString())
                     var pack = Global.ParseXML(Constants.PACK_ID,this@LearningModeActivity,false)
                    when(Constants.PACK_ID) {
                        "data_alphabet.xml"-> {
                            if(Constants.CARD != "Alphabet-Z") {
                                for (i in 0..pack!!.size) {
                                    if (pack[i].endsWith(Constants.CARD)) {
                                        var strTemp = pack[i + 1]
                                        Constants.jsonObjectData!!.getJSONArray("Alphabet").getJSONObject(i+1).remove(strTemp)
                                        Constants.jsonObjectData!!.getJSONArray("Alphabet").getJSONObject(i+1).put(strTemp,true)
                                        var str = Constants.jsonObjectData.toString()
                                        Global.saveData(baseContext,str)
                                        break
                                    }
                                }
                            }
                        }
                        "data_animals.xml"->{
                            if(Constants.CARD != "Card-Tutle") {
                                for (i in 0..pack!!.size) {
                                    if (pack[i].endsWith(Constants.CARD)) {
                                        var strTemp = pack[i + 1]
                                        Constants.jsonObjectData!!.getJSONArray("Animals").getJSONObject(i+1).remove(strTemp)
                                        Constants.jsonObjectData!!.getJSONArray("Animals").getJSONObject(i+1).put(strTemp,true)
                                        var str = Constants.jsonObjectData.toString()
                                        Global.saveData(baseContext,str)
                                        break
                                    }
                                }
                            }
                        }
                    }
                    var intent = Intent(baseContext,LessonActivity::class.java)
                    Constants.SCREEN_ID = Constants.LESSON_SCREEN_ID
                    finish()
                    startActivity(intent)
                }
                else{
                    imagePet?.postDelayed(Runnable {
                        imagePet?.setImageResource(R.drawable.cry_max)
                        imagePet?.postDelayed(Runnable {
                            imagePet?.setImageResource(R.drawable.max)
                        },1000L)
                    },0L)
                }
            }
        }
    }
}
