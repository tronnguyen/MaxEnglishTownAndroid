package cnpm.khoaluan.maxsenglishtown.activities

import android.animation.ObjectAnimator
import android.content.Intent
import android.media.MediaPlayer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import android.view.animation.AnimationUtils
import android.widget.ImageButton
import cnpm.khoaluan.maxsenglishtown.R
import cnpm.khoaluan.maxsenglishtown.global.Constants

class SchoolActivity : AppCompatActivity() {

    private var soundClick : MediaPlayer? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_school)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        soundClick = MediaPlayer()
        var afd = assets.openFd("sounds/effects/ClickButton.mp3")
        soundClick?.setDataSource(afd.fileDescriptor,afd.startOffset, afd.length)
        afd.close()
        soundClick?.prepare()
        //Init UI
        var backBtn : ImageButton = findViewById(R.id.btn_back)
        var alphabetBtn : ImageButton = findViewById(R.id.btn_alphabet)
        var animalBtn : ImageButton = findViewById(R.id.btn_animal)



        backBtn.setOnClickListener {
            soundClick?.start()
            onBackPressed()
        }
        val leftShake = AnimationUtils.loadAnimation(this, R.anim.anim_left_shake)
        leftShake.repeatCount = ObjectAnimator.INFINITE
        leftShake.repeatMode = ObjectAnimator.REVERSE
        val rightShake = AnimationUtils.loadAnimation(this, R.anim.anim_right_shake)
        rightShake.repeatCount = ObjectAnimator.INFINITE
        rightShake.repeatMode = ObjectAnimator.REVERSE
        alphabetBtn.animation = leftShake
        animalBtn.animation = rightShake
        alphabetBtn.setOnClickListener {
            soundClick?.start()
            Constants.PACK_ID = "data_alphabet.xml"
            Constants.DATA_FILE_NAME = "learn_alphabet.json"
            startActivity(Intent(baseContext,CardActivity::class.java))
        }
        animalBtn.setOnClickListener {
            soundClick?.start()
            Constants.PACK_ID = "data_animals.xml"
            Constants.DATA_FILE_NAME = "learn_animals.json"
            startActivity(Intent(baseContext,CardActivity::class.java))
        }
    }
    override fun onStop() {
        super.onStop()
        soundClick?.stop()
    }
    override fun onBackPressed() {
        startActivity(Intent(this,MapActivity::class.java))
    }
    override fun onDestroy() {
        super.onDestroy()
        soundClick?.release()
    }
}
