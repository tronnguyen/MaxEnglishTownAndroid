package cnpm.khoaluan.maxsenglishtown.activities

import android.Manifest
import android.content.Intent
import android.media.MediaPlayer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.*
import cnpm.khoaluan.maxsenglishtown.R
import cnpm.khoaluan.maxsenglishtown.global.Global
import cnpm.khoaluan.maxsenglishtown.global.Constants
import kotlinx.android.synthetic.main.activity_card.*
import org.json.JSONArray
import org.json.JSONObject
import android.animation.ObjectAnimator
import android.content.pm.PackageManager
import android.view.animation.AnimationUtils
import android.graphics.drawable.ColorDrawable
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat


class CardActivity : AppCompatActivity() {

    private var jsonArrayData : JSONArray? = null
    private var iCount = 0
    private var pack : ArrayList<String>? = null
    private var cardBtn1 : ImageButton ? = null
    private var cardBtn2 : ImageButton ? = null
    private var cardBtn3 : ImageButton ? = null
    private var cardBtn4 : ImageButton ? = null
    private var cardBtn5 : ImageButton ? = null
    private var cardBtn6 : ImageButton ? = null
    private var strCard1  = ""
    private var strCard2  = ""
    private var strCard3  = ""
    private var strCard4  = ""
    private var strCard5  = ""
    private var strCard6  = ""
    private var soundClick : MediaPlayer? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        soundClick = MediaPlayer()
        var afd = assets.openFd("sounds/effects/ClickButton.mp3")
        soundClick?.setDataSource(afd.fileDescriptor,afd.startOffset, afd.length)
        afd.close()
        soundClick?.prepare()

        //Read data
        var dataString = isOpen()
        Constants.jsonObjectData = JSONObject(dataString)
        when(Constants.PACK_ID){
            "data_alphabet.xml" ->{
                jsonArrayData = Constants.jsonObjectData!!.getJSONArray("Alphabet")
                Constants.jsonArrayData = Constants.jsonObjectData!!.getJSONArray("Alphabet")
                pack = Constants.packAlphabet
            }
            "data_animals.xml" -> {
                jsonArrayData = Constants.jsonObjectData!!.getJSONArray("Animals")
                Constants.jsonArrayData = Constants.jsonObjectData!!.getJSONArray("Animals")
                pack = Constants.packIDAnimal
            }
        }
        //Init UI
        cardBtn1  = findViewById(R.id.btn_card1)
        cardBtn2  = findViewById(R.id.btn_card2)
        cardBtn3  = findViewById(R.id.btn_card3)
        cardBtn4  = findViewById(R.id.btn_card4)
        cardBtn5  = findViewById(R.id.btn_card5)
        cardBtn6  = findViewById(R.id.btn_card6)
        var layout : LinearLayout = findViewById(R.id.layout_background)
        var title : LinearLayout = findViewById(R.id.layout_title)
        var backBtn : ImageButton = findViewById(R.id.btn_back_to_school)
        var preBtn : ImageButton = findViewById(R.id.pre_btn)
        var nextBtn : ImageButton = findViewById(R.id.next_btn)
        var guessTheWordBtn : ImageButton = findViewById(R.id.btn_guess_the_word)
        var memorymatchBtn : ImageButton = findViewById(R.id.btn_memory_match)
        var numberPageTxt : TextView = findViewById(R.id.txt_number_page)
        when(Constants.PACK_ID){
            "data_alphabet.xml" ->{
                val cd = ColorDrawable(0xFFCAF3FC.toInt())
                layout.background = cd
                var sdk = android.os.Build.VERSION.SDK_INT
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    title.background = (ContextCompat.getDrawable(baseContext, R.drawable.alphabetbanner1080) )
                } else {
                    title.background = (ContextCompat.getDrawable(baseContext, R.drawable.alphabetbanner1080))
                }
                numberPageTxt.setText("1 of 5")
            }
            "data_animals.xml" -> {
                val cd = ColorDrawable(0xFFFCE1CA.toInt())
                layout.background = cd
                var sdk = android.os.Build.VERSION.SDK_INT
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    title.background = (ContextCompat.getDrawable(baseContext, R.drawable.banner_animal) )
                } else {
                    title.background = (ContextCompat.getDrawable(baseContext, R.drawable.banner_animal))
                }
                numberPageTxt.setText("1 of 3")
            }
        }
        if(getValue(iCount,pack!![iCount])) {
            btn_card1.setImageResource(getIdFromString(pack!![iCount]))
            strCard1 = pack!![iCount]
        }
        else{
            btn_card1.isEnabled = false
            btn_card1.setImageResource(getIdFromString("lockedcardv3"))
        }
        if(getValue(iCount + 1,pack!![iCount + 1])) {
            btn_card2.setImageResource(getIdFromString(pack!![iCount + 1]))
            strCard2 = pack!![iCount + 1]
        }
        else{
            btn_card2.isEnabled = false
            btn_card2.setImageResource(getIdFromString("lockedcardv3"))
        }
        if(getValue(iCount + 2,pack!![iCount + 2])) {
            btn_card3.setImageResource(getIdFromString(pack!![iCount + 2]))
            strCard3 = pack!![iCount + 2]
        }
        else{
            btn_card3.isEnabled = false
            btn_card3.setImageResource(getIdFromString("lockedcardv3"))
        }
        if(getValue(iCount + 3,pack!![iCount + 3])) {
            btn_card4.setImageResource(getIdFromString(pack!![iCount + 3]))
            strCard4 = pack!![iCount + 3]
        }
        else{
            btn_card4.isEnabled = false
            btn_card4.setImageResource(getIdFromString("lockedcardv3"))
        }
        if(getValue(iCount + 4,pack!![iCount + 4])) {
            btn_card5.setImageResource(getIdFromString(pack!![iCount + 4]))
            strCard5 = pack!![iCount + 4]
        }
        else{
            btn_card5.isEnabled = false
            btn_card5.setImageResource(getIdFromString("lockedcardv3"))
        }
        if(getValue(iCount + 5,pack!![iCount + 5])) {
            btn_card6.setImageResource(getIdFromString(pack!![iCount + 5]))
            strCard6 = pack!![iCount + 5]
        }
        else{
            btn_card6.isEnabled = false
            btn_card6.setImageResource(getIdFromString("lockedcardv3"))
        }
        //Set Listener
        backBtn.setOnClickListener {
            soundClick?.start()
            onBackPressed()
        }
        preBtn.setOnClickListener {
            soundClick?.start()
            if(iCount>0)
                iCount -=6
            changeCardScreen()
            nextBtn.setImageResource(getIdFromString("next_enable"))
            if(iCount==0) {
                preBtn.setImageResource(getIdFromString("prev_disable"))
            }
            numberPageTxt.setText((iCount/6 + 1).toString() + " of "+ (pack!!.size/6 + 1))
        }
        nextBtn.setOnClickListener {
            soundClick?.start()
            when(Constants.PACK_ID){
                "data_alphabet.xml" ->{
                    if(iCount<24)
                        iCount +=6
                    changeCardScreen()
                    preBtn.setImageResource(getIdFromString("prev_enable"))
                    if(iCount == 24) {
                        nextBtn.setImageResource(getIdFromString("next_disable"))
                    }
                }
                "data_animals.xml" -> {
                    if(iCount<12)
                        iCount +=6
                    changeCardScreen()
                    preBtn.setImageResource(getIdFromString("prev_enable"))
                    if(iCount == 12) {
                        nextBtn.setImageResource(getIdFromString("next_disable"))
                    }
                }
            }
            numberPageTxt.setText((iCount/6 + 1).toString() + " of "+ (pack!!.size/6 + 1))
        }

        cardBtn1!!.setOnClickListener {
            soundClick?.start()
            Constants.CARD = strCard1
            nextLearning()
        }
        cardBtn2!!.setOnClickListener {
            soundClick?.start()
            Constants.CARD = strCard2
            nextLearning()
        }
        cardBtn3!!.setOnClickListener {
            soundClick?.start()
            Constants.CARD = strCard3
            nextLearning()
        }
        cardBtn4!!.setOnClickListener {
            soundClick?.start()
            Constants.CARD = strCard4
            nextLearning()
        }
        cardBtn5!!.setOnClickListener {
            soundClick?.start()
            Constants.CARD = strCard5
            nextLearning()
        }
        cardBtn6!!.setOnClickListener {
            soundClick?.start()
            Constants.CARD = strCard6
            nextLearning()
        }
        val leftShake = AnimationUtils.loadAnimation(this, R.anim.anim_left_shake)
        leftShake.repeatCount = ObjectAnimator.INFINITE
        leftShake.repeatMode = ObjectAnimator.REVERSE
        val rightShake = AnimationUtils.loadAnimation(this, R.anim.anim_right_shake)
        rightShake.repeatCount = ObjectAnimator.INFINITE
        rightShake.repeatMode = ObjectAnimator.REVERSE
        memorymatchBtn.animation = leftShake
        guessTheWordBtn.animation = rightShake
        memorymatchBtn.setOnClickListener {
            soundClick?.start()
            startActivity(Intent(baseContext, MemoryMatchActivity::class.java))
        }
        guessTheWordBtn.setOnClickListener {
            soundClick?.start()
            startActivity(Intent(baseContext, GuessTheWordActivity::class.java))
        }
    }
    private fun nextLearning(){
        if (ContextCompat.checkSelfPermission(this@CardActivity, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this@CardActivity,
                    arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), 0)
        } else {
            startActivity(Intent(baseContext, LearningModeActivity::class.java))
        }
    }
    override fun onBackPressed() {
        startActivity(Intent(this,SchoolActivity::class.java))
    }
    private fun getIdFromString(s: String): Int {
        Log.e("Debug", s)
        return resources.getIdentifier(s, "drawable", packageName)
    }
    fun changeCardScreen(){
        when(Constants.PACK_ID){
            "data_alphabet.xml" ->{
                pack = Constants.packAlphabet
                changeCardOfAlphabet()
            }
            "data_animals.xml" -> {
                pack = Constants.packIDAnimal
                changeCardOfAnimals()
            }
        }

    }
    fun changeCardOfAlphabet(){
        if(iCount < 24){
            cardBtn3!!.visibility=View.VISIBLE
            cardBtn4!!.visibility=View.VISIBLE
            cardBtn5!!.visibility=View.VISIBLE
            cardBtn6!!.visibility=View.VISIBLE
            strCard1 = pack!!.get(iCount)
            if(getValue(iCount,strCard1)) {
                cardBtn1!!.setImageResource(getIdFromString(strCard1))
                cardBtn1!!.isEnabled = true
            }
            else{
                cardBtn1!!.setImageResource(getIdFromString("lockedcardv3"))
                cardBtn1!!.isEnabled = false
            }
            strCard2 = pack!!.get(iCount + 1)
            if(getValue(iCount + 1,strCard2)) {
                cardBtn2!!.setImageResource(getIdFromString(strCard2))
                cardBtn2!!.isEnabled = true
            }
            else{
                cardBtn2!!.setImageResource(getIdFromString("lockedcardv3"))
                cardBtn2!!.isEnabled = false
            }
            strCard3 = pack!!.get(iCount + 2)
            if(getValue(iCount + 2,strCard3)) {
                cardBtn3!!.setImageResource(getIdFromString(strCard3))
                cardBtn3!!.isEnabled = true
            }
            else{
                cardBtn3!!.setImageResource(getIdFromString("lockedcardv3"))
                cardBtn3!!.isEnabled = false
            }
            strCard4 = pack!!.get(iCount + 3)
            if(getValue(iCount + 3,strCard4)) {
                cardBtn4!!.setImageResource(getIdFromString(strCard4))
                cardBtn4!!.isEnabled = true
            }
            else{
                cardBtn4!!.setImageResource(getIdFromString("lockedcardv3"))
                cardBtn4!!.isEnabled = false
            }
            strCard5 = pack!!.get(iCount + 4)
            if(getValue(iCount + 4,strCard5)) {
                cardBtn5!!.setImageResource(getIdFromString(strCard5))
                cardBtn5!!.isEnabled = true
            }
            else{
                cardBtn5!!.setImageResource(getIdFromString("lockedcardv3"))
                cardBtn5!!.isEnabled = false
            }
            strCard6 = pack!!.get(iCount + 5)
            if(getValue(iCount + 5,strCard6)) {
                cardBtn6!!.setImageResource(getIdFromString(strCard6))
                cardBtn6!!.isEnabled = true
            }
            else{
                cardBtn6!!.setImageResource(getIdFromString("lockedcardv3"))
                cardBtn6!!.isEnabled = false
            }
        }
        else if(iCount == 24){
            strCard1 = pack!!.get(iCount)
            if(getValue(iCount,strCard1)) {
                cardBtn1!!.setImageResource(getIdFromString(strCard1))
                cardBtn1!!.isEnabled = true
            }
            else{
                cardBtn1!!.setImageResource(getIdFromString("lockedcardv3"))
                cardBtn1!!.isEnabled = false
            }
            strCard2 = pack!!.get(iCount + 1)
            if(getValue(iCount + 1,strCard2)) {
                cardBtn2!!.setImageResource(getIdFromString(strCard2))
                cardBtn2!!.isEnabled = true
            }
            else{
                cardBtn2!!.setImageResource(getIdFromString("lockedcardv3"))
                cardBtn2!!.isEnabled = false
            }
            cardBtn3!!.visibility = View.INVISIBLE
            cardBtn4!!.visibility = View.INVISIBLE
            cardBtn5!!.visibility = View.INVISIBLE
            cardBtn6!!.visibility = View.INVISIBLE
        }
    }
    fun changeCardOfAnimals(){
        if(iCount < 12){
            cardBtn5!!.visibility=View.VISIBLE
            cardBtn6!!.visibility=View.VISIBLE
            strCard1 = pack!!.get(iCount)
            if(getValue(iCount,strCard1)) {
                cardBtn1!!.setImageResource(getIdFromString(strCard1))
                cardBtn1!!.isEnabled = true
            }
            else{
                cardBtn1!!.setImageResource(getIdFromString("lockedcardv3"))
                cardBtn1!!.isEnabled = false
            }
            strCard2 = pack!!.get(iCount + 1)
            if(getValue(iCount + 1,strCard2)) {
                cardBtn2!!.setImageResource(getIdFromString(strCard2))
                cardBtn2!!.isEnabled = true
            }
            else{
                cardBtn2!!.setImageResource(getIdFromString("lockedcardv3"))
                cardBtn2!!.isEnabled = false
            }
            strCard3 = pack!!.get(iCount + 2)
            if(getValue(iCount + 2,strCard3)) {
                cardBtn3!!.setImageResource(getIdFromString(strCard3))
                cardBtn3!!.isEnabled = true
            }
            else{
                cardBtn3!!.setImageResource(getIdFromString("lockedcardv3"))
                cardBtn3!!.isEnabled = false
            }
            strCard4 = pack!!.get(iCount + 3)
            if(getValue(iCount + 3,strCard4)) {
                cardBtn4!!.setImageResource(getIdFromString(strCard4))
                cardBtn4!!.isEnabled = true
            }
            else{
                cardBtn4!!.setImageResource(getIdFromString("lockedcardv3"))
                cardBtn4!!.isEnabled = false
            }
            strCard5 = pack!!.get(iCount + 4)
            if(getValue(iCount + 4,strCard5)) {
                cardBtn5!!.setImageResource(getIdFromString(strCard5))
                cardBtn5!!.isEnabled = true
            }
            else{
                cardBtn5!!.setImageResource(getIdFromString("lockedcardv3"))
                cardBtn5!!.isEnabled = false
            }
            strCard6 = pack!!.get(iCount + 5)
            if(getValue(iCount + 5,strCard6)) {
                cardBtn6!!.setImageResource(getIdFromString(strCard6))
                cardBtn6!!.isEnabled = true
            }
            else{
                cardBtn6!!.setImageResource(getIdFromString("lockedcardv3"))
                cardBtn6!!.isEnabled = false
            }
        }
        else if(iCount == 12){
            strCard1 = pack!!.get(iCount)
            if(getValue(iCount,strCard1)) {
                cardBtn1!!.setImageResource(getIdFromString(strCard1))
                cardBtn1!!.isEnabled = true
            }
            else{
                cardBtn1!!.setImageResource(getIdFromString("lockedcardv3"))
                cardBtn1!!.isEnabled = false
            }
            strCard2 = pack!!.get(iCount + 1)
            if(getValue(iCount + 1,strCard2)) {
                cardBtn2!!.setImageResource(getIdFromString(strCard2))
                cardBtn2!!.isEnabled = true
            }
            else{
                cardBtn2!!.setImageResource(getIdFromString("lockedcardv3"))
                cardBtn2!!.isEnabled = false
            }
            strCard3 = pack!!.get(iCount + 2)
            if(getValue(iCount + 2,strCard3)) {
                cardBtn3!!.setImageResource(getIdFromString(strCard3))
                cardBtn3!!.isEnabled = true
            }
            else{
                cardBtn3!!.setImageResource(getIdFromString("lockedcardv3"))
                cardBtn3!!.isEnabled = false
            }
            strCard4 = pack!!.get(iCount + 3)
            if(getValue(iCount + 3,strCard4)) {
                cardBtn4!!.setImageResource(getIdFromString(strCard4))
                cardBtn4!!.isEnabled = true
            }
            else{
                cardBtn4!!.setImageResource(getIdFromString("lockedcardv3"))
                cardBtn4!!.isEnabled = false
            }
            cardBtn5!!.visibility = View.INVISIBLE
            cardBtn6!!.visibility = View.INVISIBLE
        }
    }
    private fun isOpen() : String{
        var json = Global.getData(baseContext)
        return json!!
    }
    private fun getValue(id : Int, name : String) : Boolean{
        when(Constants.PACK_ID){
            "data_alphabet.xml" ->{
                jsonArrayData = Constants.jsonObjectData!!.getJSONArray("Alphabet")
            }
            "data_animals.xml" -> {
                jsonArrayData = Constants.jsonObjectData!!.getJSONArray("Animals")
            }
        }
        return jsonArrayData!!.getJSONObject(id).getBoolean(name)
    }
    override fun onStop() {
        super.onStop()
        soundClick?.stop()
    }
    override fun onDestroy() {
        super.onDestroy()
        soundClick?.release()
    }
}
