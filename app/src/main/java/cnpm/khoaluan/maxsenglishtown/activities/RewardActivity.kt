package cnpm.khoaluan.maxsenglishtown.activities

import android.media.MediaPlayer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import android.widget.ImageButton
import android.widget.ImageView

import cnpm.khoaluan.maxsenglishtown.R
import cnpm.khoaluan.maxsenglishtown.global.Global
import cnpm.khoaluan.maxsenglishtown.global.Constants
import cnpm.khoaluan.maxsenglishtown.model.RewardDialog
import org.json.JSONArray
import org.json.JSONObject

class RewardActivity : AppCompatActivity() {

    private var soundClick : MediaPlayer? = null
    private var badges : MutableList<ImageView>? = null
    private var jsonArrayData : JSONArray? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reward)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        //Sound
        soundClick = MediaPlayer()
        var afd = assets.openFd("sounds/effects/ClickButton.mp3")
        soundClick?.setDataSource(afd.fileDescriptor,afd.startOffset, afd.length)
        afd.close()
        soundClick?.prepare()

        //Read data
        var dataString = isOpen()
        Constants.jsonObjectRewards = JSONObject(dataString)
        jsonArrayData = Constants.jsonObjectRewards!!.getJSONArray("rewards")
        Constants.jsonArrayRewards = Constants.jsonObjectRewards!!.getJSONArray("rewards")
        //Config UI
        badges = ArrayList()
        val backBtn : ImageButton = findViewById(R.id.btn_back_to_map)
        badges?.add(findViewById(R.id.img_badge01))
        badges?.add(findViewById(R.id.img_badge02))
        badges?.add(findViewById(R.id.img_badge03))
        badges?.add(findViewById(R.id.img_badge04))
        badges?.add(findViewById(R.id.img_badge05))
        badges?.add(findViewById(R.id.img_badge06))
        badges?.add(findViewById(R.id.img_badge07))
        badges?.add(findViewById(R.id.img_badge08))
        badges?.add(findViewById(R.id.img_badge09))
        badges?.add(findViewById(R.id.img_badge10))
        badges?.add(findViewById(R.id.img_badge11))
        badges?.add(findViewById(R.id.img_badge12))
        //Set listener
        backBtn.setOnClickListener {
            soundClick?.start()
            onBackPressed()
        }
        var iCount = 0
        var type: Boolean
        badges?.forEach {
            if(getValue(iCount,"trophy" + (iCount+1).toString())){
                it.setImageResource(getIdFromString("b"+(iCount+1).toString()))
                type = true
            }
            else{
                type = false
            }
            it.setOnClickListener {
                type = getValue(badges?.indexOf(it)!!,"trophy" + (badges?.indexOf(it)!! + 1).toString())
                var dialog : RewardDialog = RewardDialog(this,type, badges?.indexOf(it)!! + 1)
                dialog.show()
            }
            iCount++
        }
    }
    fun save(index: Int) {
        Constants.jsonObjectRewards!!.getJSONArray("rewards").getJSONObject(index).remove("trophy" + (index + 1).toString())
        Constants.jsonObjectRewards!!
                .getJSONArray("Animals")
                .getJSONObject(index)
                .put("trophy" + (index + 1).toString(), true)
        var str = Constants.jsonObjectRewards.toString()
        Global.saveDataRewards(baseContext, str)
    }
    private fun getIdFromString(s: String): Int {
        Log.e("Debug", s)
        return resources.getIdentifier(s, "drawable", packageName)
    }
    private fun isOpen() : String{
        var json = Global.getDataRewards(baseContext)
        return json!!
    }
    private fun getValue(id : Int, name : String) : Boolean{
        jsonArrayData = Constants.jsonObjectRewards!!.getJSONArray("rewards")
        return jsonArrayData!!.getJSONObject(id).getBoolean(name)
    }
    override fun onStop() {
        super.onStop()
        soundClick?.stop()
    }
    override fun onDestroy() {
        super.onDestroy()
        soundClick?.release()
    }
}
