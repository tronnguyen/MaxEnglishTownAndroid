package cnpm.khoaluan.maxsenglishtown.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import cnpm.khoaluan.maxsenglishtown.R
import cnpm.khoaluan.maxsenglishtown.application.ImageTargets
import cnpm.khoaluan.maxsenglishtown.global.Constants
import cnpm.khoaluan.maxsenglishtown.springy.facebook.rebound.SpringAnimationType
import cnpm.khoaluan.maxsenglishtown.springy.facebook.rebound.SpringyAnimator
import com.bumptech.glide.Glide
import com.facebook.rebound.SpringSystem
import com.vuforia.State
import kotlinx.android.synthetic.main.activity_library_input.*

class LibraryInputActivity : ImageTargets() {

    private var flag = false
    override fun onCreate(savedInstanceState: Bundle?) {
        SetUILayout(R.layout.activity_library_input)
        getmUILinearLayout().setBackgroundResource(R.drawable.background_library_chose)
        super.onCreate(savedInstanceState)
        Constants.SpringSystemManager = SpringSystem.create()
        getmDatasetStrings().add("data_library.xml")
        img_book.isEnabled = false
        img_book.setOnClickListener{
            startActivity(Intent(baseContext,LibraryActivity::class.java))
        }
        btn_back.setOnClickListener{
            onBackPressed()
        }
    }
    override fun onBackPressed() {
        if(isLoaded) {
//            super.onBackPressed()
            finish()
        }
    }

    override fun getClassName(): String {
        return "LibraryInputActivity"
    }
    override fun onResume() {
        super.onResume()
        Constants.SpringSystemManager = SpringSystem.create()
        flag = false
    }
    override fun onVuforiaUpdate(state: State?) {
        super.onVuforiaUpdate(state)
        if(state!= null){
            var number = state.numTrackableResults
            for (tIdx in 0 until number) {
                val result = state.getTrackableResult(tIdx)
                val trackable = result.trackable
                var tempCard = trackable.name.toString()
                if (tempCard.endsWith("zoo") && !flag) {
                    moveUp(img_book,"book")
                    img_book.isEnabled = true
                    flag = true
                }
            }
        }
    }
    private fun moveUp(imageView : ImageView, strRes: String){
        var height = resources
                .displayMetrics
                .heightPixels.toFloat()
        val spring3 = SpringyAnimator(SpringAnimationType.TRANSLATEY,
                0f, 0f, height/2, 0f, Constants.SpringSystemManager)

        imageView.postDelayed(Runnable {
            imageView.visibility = View.INVISIBLE
        },0L)

        imageView.postDelayed(Runnable {
            spring3.startSpring(imageView)
            imageView.visibility = View.VISIBLE
        }, (500).toLong())
        imageView.postDelayed(Runnable {
            Glide.with(baseContext)
                    .load(GetIdFromString(strRes))
                    .into(imageView)
        },0L)

    }
    //GetIdFromString
    private fun GetIdFromString(s: String): Int? {
        Log.e("Debug", s)
        return resources.getIdentifier(s, "drawable", packageName)
    }
}
