package cnpm.khoaluan.maxsenglishtown.activities

import android.Manifest
import android.animation.ObjectAnimator
import android.content.Intent
import android.graphics.drawable.Drawable
import android.media.MediaPlayer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageButton
import android.widget.ImageView
import cnpm.khoaluan.maxsenglishtown.R
import cnpm.khoaluan.maxsenglishtown.global.Constants
import cnpm.khoaluan.maxsenglishtown.minigame.FindAlpha
import java.io.InputStream
import android.graphics.drawable.AnimationDrawable
import android.view.WindowManager
import android.view.animation.AnimationUtils
import cnpm.khoaluan.maxsenglishtown.model.RewardDialog
import android.Manifest.permission
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.support.v4.app.ActivityCompat
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat




class MapActivity : AppCompatActivity() {

    private var soundClick : MediaPlayer? = null
    var soundBackground: MediaPlayer? = null
    override fun onResume() {
        super.onResume()
        var iCount = 11
        while(iCount >= 0) {
            if (Constants.rewards[iCount]) {
                var dialog = RewardDialog(this, true, iCount + 1)
                dialog.show()
                Constants.saveDataRewards(iCount, baseContext)
                Constants.rewards[iCount] = false
            }
            iCount--
        }
    }
    override fun onRestart() {
        super.onRestart()
        soundBackground?.start()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)

        soundClick = MediaPlayer()
        var afd = assets.openFd("sounds/effects/ClickButton.mp3")
        soundClick?.setDataSource(afd.fileDescriptor,afd.startOffset, afd.length)
        afd.close()
        soundClick?.prepare()


        soundBackground = MediaPlayer()
        var afd2 = assets.openFd("sounds/effects/BackgroundMusic.mp3")
        soundBackground?.setDataSource(afd2.fileDescriptor,afd2.startOffset, afd2.length)
        afd2.close()
        soundBackground?.prepare()
        soundBackground?.start()
        //Init UI
        var backBtn : ImageView = findViewById(R.id.btn_back_to_home)
        var schoolBtn : ImageView = findViewById(R.id.btn_school)
        var libraryBtn : ImageView  = findViewById(R.id.btn_library)
        var cafeBtn : ImageView = findViewById(R.id.btn_cafe)
        var parkBtn : ImageView = findViewById(R.id.btn_park)
        var rewarBtn : ImageButton = findViewById(R.id.btn_reward)

        schoolBtn.setImageResource(R.drawable.school_animation)
        val schoolAnimation = schoolBtn.drawable as AnimationDrawable
        schoolAnimation.start()

        cafeBtn.setImageResource(R.drawable.cafe_animation)
        val cafeAnimation = cafeBtn.drawable as AnimationDrawable
        cafeAnimation.start()

        libraryBtn.setImageResource(R.drawable.library_animation)
        val libraryAnimation = libraryBtn.drawable as AnimationDrawable
        libraryAnimation.start()

        parkBtn.setImageResource(R.drawable.park_animation)
        val parkAnimation = parkBtn.drawable as AnimationDrawable
        parkAnimation.start()

        //Set listener
        cafeBtn.setOnClickListener{
            if (ContextCompat.checkSelfPermission(this@MapActivity, Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this@MapActivity,
                        arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), 0)
            } else {
                startActivity(Intent(MapActivity@this,CafeActivity::class.java))
            }

        }
        backBtn.setOnClickListener {
            soundClick?.start()
            onBackPressed()
        }
        schoolBtn.setOnClickListener {
            soundClick?.start()
            startActivity(Intent(baseContext,SchoolActivity::class.java))
        }
        libraryBtn.setOnClickListener {
            soundClick?.start()
            if (ContextCompat.checkSelfPermission(this@MapActivity, Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this@MapActivity,
                        arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), 0)
            } else {
                startActivity(Intent(baseContext, LibraryInputActivity::class.java))
            }
        }
        parkBtn.setOnClickListener {
            soundClick?.start()
            if (ContextCompat.checkSelfPermission(this@MapActivity, Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this@MapActivity,
                        arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), 0)
            } else {
                startActivity(Intent(baseContext, FindAlpha::class.java))
            }
        }
        val anim = AnimationUtils.loadAnimation(this, R.anim.anim_shake_scale)
        anim.repeatCount = ObjectAnimator.INFINITE
        anim.repeatMode = ObjectAnimator.REVERSE
        rewarBtn.animation = anim
        rewarBtn.setOnClickListener {
            soundClick?.start()
            startActivity(Intent(baseContext,RewardActivity::class.java))
        }

    }
    private fun getDrawable(name :String, type : Boolean) : Drawable {
        var ims: InputStream?
        if(type){
            ims = assets.open(Constants.PATH_IMAGES_OTHERS + name)
        }
        else{
            ims = assets.open(Constants.PATH_IMAGES_BUTTON + name)
        }
        // load image as Drawable
        val dra = Drawable.createFromStream(ims, null)
        return dra
    }
    override fun onBackPressed() {
        startActivity(Intent(this,WelcomeActivity::class.java))
    }
    override fun onStop() {
        super.onStop()
        soundClick?.stop()
        soundBackground?.stop()
    }
    override fun onDestroy() {
        super.onDestroy()
        soundClick?.release()
        soundBackground?.release()
    }
}
