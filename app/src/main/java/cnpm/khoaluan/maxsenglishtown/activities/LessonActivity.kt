package cnpm.khoaluan.maxsenglishtown.activities

import android.animation.ObjectAnimator
import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.media.MediaPlayer
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.animation.AnimationUtils
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import cnpm.khoaluan.maxsenglishtown.R
import cnpm.khoaluan.maxsenglishtown.global.Global
import cnpm.khoaluan.maxsenglishtown.global.Constants
import cnpm.khoaluan.maxsenglishtown.model.MyRecognitionListener
import cnpm.khoaluan.maxsenglishtown.model.RewardDialog
import java.util.*
import kotlin.collections.ArrayList
import android.os.Handler
import android.os.Looper
import android.widget.RelativeLayout


class LessonActivity : AppCompatActivity() {

    private var words: Dictionary<String, String> = Hashtable()
    private var txtCard : TextView? = null
    private var layoutCardView : RelativeLayout? = null
    private var txtSample : TextView? = null
    private var txtRepeat : TextView? = null
    private var imageSample : ImageView? = null
    private var imageMicro : ImageView? = null
    private var imageFooder : ImageView? = null
    private var imageTitle : ImageView? = null
    private var btnBackTolearn : ImageButton? = null
    private var recognition : MyRecognitionListener? = null
    private var soundCard : MediaPlayer? = null
    private var soundRepeat : MediaPlayer? = null
    private var soundGreatJob : MediaPlayer? = null
    private var soundExample : MediaPlayer? = null
    private var activity : Activity? = null
    private var handler : Handler? = null
    private var runableCard1 : Runnable? = null
    private var runableCard2 : Runnable? = null
    private var runableCard3 : Runnable? = null
    private var runableCard4 : Runnable? = null
    private var runableCard5 : Runnable? = null
    private var runableCard6 : Runnable? = null
    private var runableCard7 : Runnable? = null
    private var runableCard8 : Runnable? = null
    private var runableCard9 : Runnable? = null
    private var runableCard10 : Runnable? = null
    private var runableCard11 : Runnable? = null
    private var runableCard12 : Runnable? = null
    private var scale = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lesson)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        activity = this
        handler = Handler(Looper.getMainLooper())
        //Init UI
        val shake = AnimationUtils.loadAnimation(this, R.anim.anim_shake)
        shake.repeatCount = ObjectAnimator.INFINITE
        shake.repeatMode = ObjectAnimator.REVERSE
        txtCard = findViewById(R.id.txt_card)
        layoutCardView = findViewById(R.id.layout_cardview)
        txtCard!!.animation = shake
        txtRepeat = findViewById(R.id.txt_repeat)
        txtSample = findViewById(R.id.txt_sample)
        imageTitle = findViewById(R.id.image_title_card)
        imageFooder = findViewById(R.id.image_fooder)
        imageSample = findViewById(R.id.image_sampe)
        imageMicro  = findViewById(R.id.image_micro_phone)

        btnBackTolearn = findViewById(R.id.btn_back_to_learn)
        //Load data
        words = Global.ParseXMLToHashTable("Alphabet.xml",this)!!
        recognition = MyRecognitionListener(this,"hello")
        //Config UI

        var card = Constants.CARD
        var word = "ABC"
        var footerText = ""
        var titleText = ""
        var sampleText = ""
        var sampleRes = ""
        //Switch Alphabet and others
        when(Constants.PACK_ID) {
            "data_alphabet.xml"-> {
                word = card.toLowerCase().substring(card.length - 1)
                soundCard = MediaPlayer()
                var afd = assets.openFd("sounds/alphabets/"+word+".mp3")
                soundCard?.setDataSource(afd.fileDescriptor,afd.startOffset, afd.length)
                afd.close()
                soundCard?.prepare()
                footerText = "pronunciationbanner"
                titleText ="title_alphabet"
                sampleRes =  words[card.substring(card.length - 1).toUpperCase()].toLowerCase()+"_"+word
                sampleText = words[word.toUpperCase()]
                scale = 1
            }
            "data_animals.xml"->{
                word = card.toLowerCase().substring(5)
                soundCard = MediaPlayer()
                var afd = assets.openFd("sounds/animals/"+word+".mp3")
                soundCard?.setDataSource(afd.fileDescriptor,afd.startOffset, afd.length)
                sampleRes ="animal_"+card.toLowerCase().substring(5).toLowerCase()

                footerText = "footer_animal"
                titleText ="title_animal"
                sampleText = card.substring(5)
                scale = 2
            }
        }

        imageTitle?.setImageResource(getIdFromString(titleText))
        imageFooder?.setImageResource(getIdFromString(footerText))
        imageSample?.setImageResource(getIdFromString(sampleRes))
        txtCard?.text = word.toUpperCase()
        var displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        var width = displayMetrics.widthPixels/displayMetrics.density
        var height = displayMetrics.heightPixels/displayMetrics.density
        if(width.toInt()*16 == height.toInt()*9) {
            txtCard?.textSize = 180f/scale
        }
        else if(width.toInt()*4 == height.toInt()*3){
            txtCard?.textSize = 300f/scale
        }
        else{
            txtCard?.textSize = 250f/scale
        }
        txtRepeat?.text = "Repeat after me"
        txtSample?.text = sampleText
        txtCard?.setTextColor(Color.parseColor("#FD4A0C"))
        recognition?.SetWord(word)
        //Sound
        soundRepeat = MediaPlayer()
        var afd = assets.openFd("sounds/alphabets/repeat.mp3")
        soundRepeat?.setDataSource(afd.fileDescriptor,afd.startOffset, afd.length)
        afd.close()
        soundRepeat?.prepare()

        soundGreatJob = MediaPlayer()
        var afd2 = assets.openFd("sounds/alphabets/greatjob.mp3")
        soundGreatJob?.setDataSource(afd2.fileDescriptor,afd2.startOffset, afd2.length)
        afd2.close()
        soundGreatJob?.prepare()
    }
    override fun onBackPressed() {
        startActivity(Intent(this,CardActivity::class.java))
    }
    private fun getIdFromString(s: String): Int {
        Log.e("Debug", s)
        return resources.getIdentifier(s, "drawable", packageName)
    }
    override fun onResume() {
        super.onResume()
        when(Constants.PACK_ID) {
            "data_alphabet.xml"-> {
                var word = Constants.CARD.toLowerCase().substring(Constants.CARD.length - 1)
                soundCard = MediaPlayer()
                var afd = assets.openFd("sounds/alphabets/"+word+".mp3")
                soundCard?.setDataSource(afd.fileDescriptor,afd.startOffset, afd.length)
                afd.close()
                soundCard?.prepare()
            }
            "data_animals.xml"->{
                var word = Constants.CARD.toLowerCase().substring(5)
                soundCard = MediaPlayer()
                var afd = assets.openFd("sounds/animals/"+word+".mp3")
                soundCard?.setDataSource(afd.fileDescriptor,afd.startOffset, afd.length)
                soundCard?.prepare()
            }
        }
        soundRepeat = MediaPlayer()
        var afd = assets.openFd("sounds/alphabets/repeat.mp3")
        soundRepeat?.setDataSource(afd.fileDescriptor,afd.startOffset, afd.length)
        afd.close()
        soundRepeat?.prepare()

        txtCard?.visibility = View.VISIBLE
        layoutCardView?.visibility = View.VISIBLE
        imageMicro?.visibility = View.INVISIBLE
        txtRepeat?.visibility = View.INVISIBLE
        txtSample?.visibility = View.GONE
        imageSample?.visibility = View.GONE
        runableCard1 = Runnable {
            //Play pronunciation
            soundCard?.start()
            txtCard?.setTextColor(Color.parseColor("#FD4A0C"))
        }
        handler?.postDelayed(runableCard1, (500).toLong())
        runableCard2 = Runnable {
            //Play pronunciation
            soundCard?.start()
            txtCard?.setTextColor(Color.BLUE)
        }
        handler?.postDelayed(runableCard2, (1500).toLong())
        runableCard3 = Runnable {
            //Play pronunciation
            txtCard?.setTextColor(Color.parseColor("#FD0C5B"))
            txtRepeat?.visibility = View.VISIBLE
            imageMicro?.visibility = View.VISIBLE
        }
        handler?.postDelayed(runableCard3, (2500).toLong())
        runableCard4 = Runnable {
            //Play pronunciation
            soundRepeat?.start()
        }
        handler?.postDelayed(runableCard4, (3500).toLong())
        runableCard5 = Runnable {
            //Play pronunciation
            soundCard?.start()
        }
        handler?.postDelayed(runableCard5, (5500).toLong())
        runableCard6 = Runnable {
            //Play pronunciation
            soundCard?.start()
            recognition?.StartListening()
            SomeTask().execute()
        }
        handler?.postDelayed(runableCard6, (7500).toLong())

    }

    override fun onStop() {
        super.onStop()
        soundCard?.stop()
        soundGreatJob?.stop()
        soundRepeat?.stop()
        soundExample?.stop()
        handler?.removeCallbacks(runableCard1)
        handler?.removeCallbacks(runableCard2)
        handler?.removeCallbacks(runableCard3)
        handler?.removeCallbacks(runableCard4)
        handler?.removeCallbacks(runableCard5)
        handler?.removeCallbacks(runableCard6)
        handler?.removeCallbacks(runableCard7)
        handler?.removeCallbacks(runableCard8)
        handler?.removeCallbacks(runableCard9)
        handler?.removeCallbacks(runableCard10)
        handler?.removeCallbacks(runableCard11)
        handler?.removeCallbacks(runableCard12)
    }
    override fun onDestroy() {
        super.onDestroy()
        soundCard?.release()
        soundGreatJob?.release()
        soundRepeat?.release()
        soundExample?.release()
    }
    private inner class SomeTask : AsyncTask<Void, Int, Void>() {
        override fun doInBackground(vararg params: Void?): Void? {
            while(!recognition!!.EngOfSpeech()){
                if(recognition!!.isWrong()) {
                    publishProgress(0)
                    return null
                }
                if(recognition!!.Match()){
                    publishProgress(1)
                    return null
                }
            }
            if(recognition!!.EngOfSpeech()){
                publishProgress(0)
            }


            return null
        }

        override fun onProgressUpdate(vararg values: Int?) {
            super.onProgressUpdate(*values)
            if(values[0]==0){
                txtCard!!.animation = null
                layoutCardView?.visibility = View.GONE
                txtCard?.visibility = View.GONE
                imageMicro?.visibility = View.GONE
                txtRepeat?.visibility = View.GONE
                txtSample?.visibility = View.VISIBLE
                imageSample?.visibility = View.VISIBLE
                runableCard7 = Runnable {
                    var pack: ArrayList<String>?
                    if(Constants.PACK_ID.endsWith("data_alphabet.xml")){
                        soundExample = MediaPlayer()
                        var afd = assets.openFd("sounds/examples/ex_" + txtSample?.text.toString().toLowerCase() + ".mp3")
                        soundExample?.setDataSource(afd.fileDescriptor,afd.startOffset, afd.length)
                        afd.close()
                        soundExample?.prepare()
                        runableCard11 = Runnable {
                            //Sound
                            soundExample?.start()
                        }
                        runableCard12 = Runnable {
                            //Sound
                            soundExample?.start()
                        }
                        handler?.postDelayed(runableCard11,1500L)
                        if(!Constants.getValue(1,"trophy2") && "alphabet_a".toString().endsWith(Constants.CARD)){
//                            Constants.saveDataRewards(1,baseContext)
//                            var dialog : RewardDialog = RewardDialog(activity!!,true, 2)
//                            dialog.show()
                            Constants.rewards[1] = true
                        }
                        if(!Constants.getValue(2,"trophy3") && "alphabet_j".toString().endsWith(Constants.CARD)){
//                            Constants.saveDataRewards(2,baseContext)
//                            var dialog : RewardDialog = RewardDialog(activity!!,true, 3)
//                            dialog.show()
                            Constants.rewards[2] = true
                        }
                        if(!Constants.getValue(3,"trophy4") && "alphabet_t".toString().endsWith(Constants.CARD)){
//                            Constants.saveDataRewards(3,baseContext)
//                            var dialog : RewardDialog = RewardDialog(activity!!,true, 4)
//                            dialog.show()
                            Constants.rewards[3] = true
                        }
                        if(!Constants.getValue(4,"trophy5") && "alphabet_z".toString().endsWith(Constants.CARD)){
//                            Constants.saveDataRewards(4,baseContext)
//                            var dialog : RewardDialog = RewardDialog(activity!!,true, 5)
//                            dialog.show()
                            Constants.rewards[4] = true
                        }
                        //pack = Constants.packAlphabet
                        runableCard8 = Runnable {
                            startActivity(Intent(baseContext,PracticeActivity::class.java))
                        }
                        handler?.postDelayed(runableCard8,3000L)
                    }
                    else{
                        pack = Constants.packIDAnimal
                        for (i in 0..pack!!.size) {
                            if (pack.get(i).endsWith(Constants.CARD)) {
                                Constants.CARD = pack.get(i + 1)
                                break
                            }
                        }

                        if(!Constants.getValue(5,"trophy6") && "card_bee".toString().endsWith(Constants.CARD)){
//                            Constants.saveDataRewards(5,baseContext)
//                            var dialog : RewardDialog = RewardDialog(activity!!,true, 6)
//                            dialog.show()
                            Constants.rewards[5] = true
                        }
                        if(!Constants.getValue(6,"trophy7") && "card_hamster".toString().endsWith(Constants.CARD)){
//                            Constants.saveDataRewards(6,baseContext)
//                            var dialog : RewardDialog = RewardDialog(activity!!,true, 7)
//                            dialog.show()
                            Constants.rewards[6] = true
                        }
                        if(!Constants.getValue(7,"trophy8") && "card_turtle".toString().endsWith(Constants.CARD)){
//                            Constants.saveDataRewards(7,baseContext)
//                            var dialog : RewardDialog = RewardDialog(activity!!,true, 8)
//                            dialog.show()
                            Constants.rewards[7] = true
                        }
                        runableCard9 = Runnable {
                            startActivity(Intent(baseContext, CardActivity::class.java))
                        }
                        handler?.postDelayed(runableCard9,3000)

                    }

                }
                handler?.postDelayed(runableCard7,1000L)

            }
            else if(values[0]==1){
                txtCard!!.animation = null
                txtCard?.visibility = View.GONE
                layoutCardView?.visibility = View.GONE
                imageMicro?.visibility = View.GONE
                txtRepeat?.visibility = View.GONE
                txtSample?.visibility = View.VISIBLE
                imageSample?.visibility = View.VISIBLE
                if(Constants.PACK_ID.endsWith("data_alphabet.xml")){
                    soundExample = MediaPlayer()
                    var afd = assets.openFd("sounds/examples/ex_" + txtSample?.text.toString().toLowerCase() + ".mp3")
                    soundExample?.setDataSource(afd.fileDescriptor,afd.startOffset, afd.length)
                    afd.close()
                    soundExample?.prepare()
                    runableCard11 = Runnable {
                        //Sound
                        soundExample?.start()
                    }
                    runableCard12 = Runnable {
                        //Sound
                        soundExample?.start()
                    }
                    handler?.postDelayed(runableCard11,1000L)
                    handler?.postDelayed(runableCard12,2000L)
                    runableCard10 = Runnable {
                        if(!Constants.getValue(1,"trophy2") && "alphabet_a".toString().endsWith(Constants.CARD)){
                            Constants.saveDataRewards(1,baseContext)
                            var dialog : RewardDialog = RewardDialog(activity!!,true, 2)
                            dialog.show()
                        }
                        if(!Constants.getValue(5,"trophy6") && "card_bee".toString().endsWith(Constants.CARD)){
                            Constants.saveDataRewards(5,baseContext)
                            var dialog : RewardDialog = RewardDialog(activity!!,true, 6)
                            dialog.show()
                        }
                        startActivity(Intent(baseContext,PracticeActivity::class.java))
                    }
                    handler?.postDelayed(runableCard10,3000)
                }
            }
        }
    }

}
