package cnpm.khoaluan.maxsenglishtown.activities

import android.media.MediaPlayer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import cnpm.khoaluan.maxsenglishtown.R
import cnpm.khoaluan.maxsenglishtown.global.Global
import cnpm.khoaluan.maxsenglishtown.global.Constants
import com.bumptech.glide.Glide
import java.util.*

class GuessTheWordActivity : AppCompatActivity() {

    private var btnBlue : ImageButton? = null
    private var btnOrange : ImageButton? = null
    private var btnPurple : ImageButton? = null
    private var txtBlue : TextView? = null
    private var txtOrange : TextView? = null
    private var txtPurple : TextView? = null
    private var words: Dictionary<String, String> = Hashtable()
    private var list : List<String>? = null
    private var answerWord = ""
    private var imagePicture : ImageView? = null
    private var strBlue = ""
    private var strOrange = ""
    private var strPurple = ""
    private var soundRight : MediaPlayer? = null
    private var soundWrong : MediaPlayer? = null
    private var soundClick : MediaPlayer? = null
    private var countTime = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guess_the_word)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        //Sound
        soundClick = MediaPlayer()
        var afd3 = assets.openFd("sounds/effects/ClickButton.mp3")
        soundClick?.setDataSource(afd3.fileDescriptor,afd3.startOffset, afd3.length)
        afd3.close()
        soundClick?.prepare()

        soundRight = MediaPlayer()
        var afd = assets.openFd("sounds/effects/Right.mp3")
        soundRight?.setDataSource(afd.fileDescriptor,afd.startOffset, afd.length)
        afd.close()
        soundRight?.prepare()

        soundWrong = MediaPlayer()
        var afd2 = assets.openFd("sounds/effects/Wrong.mp3")
        soundWrong?.setDataSource(afd2.fileDescriptor,afd2.startOffset, afd2.length)
        afd2.close()
        soundWrong?.prepare()
        //Load data
        when(Constants.PACK_ID){
            "data_alphabet.xml"->{
                words = Global.ParseXMLToHashTable("Alphabet.xml",this)!!
            }
            "data_animals.xml"->{
                words = Global.ParseXMLToHashTable("Animals.xml",this)!!
            }
        }

        list = ArrayList<String>()
        list = words.keys().toList()
        answerWord = words.get(list!![0])
        strBlue = words.get(list!![0])
        strOrange = words.get(list!![1])
        strPurple = words.get(list!![2])
        //Init UI
        var backBtn : ImageView = findViewById(R.id.btn_back_to_card)
        imagePicture = findViewById(R.id.image_view)
        btnBlue = findViewById(R.id.btn_blue)
        btnOrange = findViewById(R.id.btn_orange)
        btnPurple = findViewById(R.id.btn_purple)
        txtBlue = findViewById(R.id.txt_blue)
        txtOrange = findViewById(R.id.txt_orange)
        txtPurple = findViewById(R.id.txt_purple)
        //Config UI
        txtBlue?.text = strBlue
        txtOrange?.text = strOrange
        txtPurple?.text = strPurple
        Glide.with(baseContext)
                .load(getIdFromString(words[list!![0]].toLowerCase()))
                .into(imagePicture!!)
        //Set Listener
        backBtn.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                soundClick?.start()
                onBackPressed()
            }
        })
        btnBlue?.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                if(strBlue == answerWord){
                    countTime++
                    if(countTime == 9 && !Constants.getValue(10,"trophy11")){
//                        Constants.saveDataRewards(10,baseContext)
//                        var dialog : RewardDialog = RewardDialog(this@GuessTheWordActivity,true, 11)
//                        dialog.show()
                        Constants.rewards[10] = true
                    }
                    //Play sound
                    soundRight?.start()
                    var id=0
                    when(Constants.PACK_ID){
                        "data_alphabet.xml"->{
                            id = Random().nextInt(23)
                        }
                        "data_animals.xml"->{
                            id = Random().nextInt(13)
                        }
                    }
                    strBlue = getWord(words[list!![id]])
                    strOrange = getWord(words[list!![id + 1]])
                    strPurple = getWord(words[list!![id + 2]])
                    txtBlue?.text = strBlue
                    txtOrange?.text = strOrange
                    txtPurple?.text = strPurple
                    var chose = id + Random().nextInt(3)
                    answerWord = getWord(words[list!![chose]])
                    Glide.with(baseContext)
                            .load(getIdFromString(words[list!![chose]].toLowerCase()))
                            .into(imagePicture!!)

                }
                else{
                    //Play sound
                    soundWrong?.start()
                }
            }
        })
        btnOrange?.setOnClickListener {
            if(strOrange == answerWord){
                countTime++
                if(countTime == 9 && !Constants.getValue(10,"trophy11")){
//                        Constants.saveDataRewards(10,baseContext)
//                        var dialog : RewardDialog = RewardDialog(this@GuessTheWordActivity,true, 11)
//                        dialog.show()
                    Constants.rewards[10] = true
                }
                //Play sound
                soundRight?.start()
                var id=0
                when(Constants.PACK_ID){
                    "data_alphabet.xml"->{
                        id = Random().nextInt(23)
                    }
                    "data_animals.xml"->{
                        id = Random().nextInt(13)
                    }
                }
                strBlue = getWord(words[list!![id]])
                strOrange = getWord(words[list!![id + 1]])
                strPurple = getWord(words[list!![id + 2]])
                txtBlue?.text = strBlue
                txtOrange?.text = strOrange
                txtPurple?.text = strPurple
                var chose = id + Random().nextInt(3)
                answerWord = getWord(words[list!![chose]])
                Glide.with(baseContext)
                        .load(getIdFromString(words[list!![chose]].toLowerCase()))
                        .into(imagePicture!!)

            } else{
                //Play sound
                soundWrong?.start()
            }
        }
        btnPurple?.setOnClickListener {
            if(strPurple == answerWord){
                countTime++
                if(countTime == 9 && !Constants.getValue(10,"trophy11")){
//                        Constants.saveDataRewards(10,baseContext)
//                        var dialog : RewardDialog = RewardDialog(this@GuessTheWordActivity,true, 11)
//                        dialog.show()
                    Constants.rewards[10] = true
                }
                //Play sound
                soundRight?.start()
                var id=0
                when(Constants.PACK_ID){
                    "data_alphabet.xml"->{
                        id = Random().nextInt(23)
                    }
                    "data_animals.xml"->{
                        id = Random().nextInt(13)
                    }
                }
                strBlue = getWord(words[list!![id]])
                strOrange = getWord(words[list!![id + 1]])
                strPurple = getWord(words[list!![id + 2]])
                txtBlue?.text = strBlue
                txtOrange?.text = strOrange
                txtPurple?.text = strPurple
                var chose = id + Random().nextInt(3)
                answerWord = getWord(words[list!![chose]])
                Glide.with(baseContext)
                        .load(getIdFromString(words[list!![chose]].toLowerCase()))
                        .into(imagePicture!!)

            } else{
                //Play sound
                soundWrong?.start()
            }
        }
    }
    private fun getWord(str :String) : String {
        return when {
            str.contains("4") -> str.replace("4","")
            str.contains("5") -> str.replace("5","")
            else -> str
        }
    }
    private fun getIdFromString(s: String): Int? {
        Log.e("Debug", s)
        return resources.getIdentifier(s, "drawable", packageName)
    }
    override fun onStop() {
        super.onStop()
        soundRight?.stop()
        soundWrong?.stop()
        soundClick?.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        soundRight?.release()
        soundWrong?.release()
        soundClick?.stop()
    }
}
